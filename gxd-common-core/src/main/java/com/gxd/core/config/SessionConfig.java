package com.gxd.core.config;

import com.gxd.commons.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.CookieHttpSessionStrategy;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @Author:gxd
 * @Description:
 * @Date: 14:25 2018/3/29
 * @Modified By:
 */
@Configuration
public class SessionConfig {
    @SuppressWarnings("SpringJavaAutowiringInspection") //加这个注解让IDE 不报: Could not autowire
    @Autowired
    @Qualifier("commConstConfig")
    private CommConstConfig commConstConfig;
    @Bean
    public CookieHttpSessionStrategy cookieHttpSessionStrategy() {
        CookieHttpSessionStrategy strategy = new CookieHttpSessionStrategy();
        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
        cookieSerializer.setCookieName("SSOSESSIONID");//cookies名称
        cookieSerializer.setCookieMaxAge(commConstConfig.getCookieMaxAge());//过期时间(秒)
        //存储路径
        cookieSerializer.setCookiePath("/");
        cookieSerializer.setUseHttpOnlyCookie(true);
        //如果是域名需设置一级域名
        if(StringUtils.isNotBlank(commConstConfig.getDomainName()) && !commConstConfig.getDomainName().equals("test")) {
            cookieSerializer.setDomainName(commConstConfig.getDomainName());
        }
        //cookieSerializer.setDomainNamePattern("(^192.168.50.3)[3-7]{1}");
        strategy.setCookieSerializer(cookieSerializer);
        return strategy;
    }
}
