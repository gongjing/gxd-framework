package com.gxd.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Author:gxd
 * @Description:
 * @Date: 21:10 2018/4/12
 * @Modified By:
 */
@Component("commConstConfig")
@RefreshScope
public class CommConstConfig {

    /**
     * 单点登录服务器
     */
    @Value("${gxd.sso.serverDomain:http://192.168.50.36:8099}")
    private String serverDomain;
    /**
     * 单点登录地址
     */
    @Value("${gxd.sso.serverLoginUrl:/sso/login}")
    private String serverLoginUrl;
    /**
     * 单点登录认证地址，获取加密信息
     */
    @Value("${gxd.sso.authUrl: /sso/doAuth/}")
    private String authUrl;

    /**
     * 单点登录根据token获取用户信息,明文信息
     */
    @Value("${gxd.sso.tokenUrl:/sso/token/}")
    private String tokenUrl;
    /**
     * 子系统编码
     */
    @Value("${gxd.sso.systemCode:systemamp}")
    private String systemCode;

    /**
     * 注册地址
     */
    @Value("${gxd.sso.regUrl:regUrl}")
    private String regUrl;

    /**
     * 子系统注册令牌
     */
    @Value("${gxd.sso.sysRegisterToken:sysRegisterToken}")
    private String sysRegisterToken;
    /**
     * 秘钥
     */
    @Value("${gxd.sso.signingKey:signingKey}")
    private String signingKey;
    /**
     * 一级域名
     */
    @Value("${gxd.sso.domainName:.test}")
    private String domainName;

    /**
     * 超级用户id
     */
    @Value("${gxd.sso.adminId:0}")
    private String adminId;

    /**
     * rest超时设置
     */
    @Value("${gxd.sso.readTimeout:30000}")
    private int readTimeout;

    /**
     * REST超时配置
     */
    @Value("${gxd.sso.connectTimeout:30000}")
    private int connectTimeout;

    /**
     * 是否启用代理
     */
    @Value("${gxd.sso.proxy:false}")
    private Boolean proxy;
    /**
     * 代理主机地址
     */
    @Value("${gxd.sso.host:127.0.0.1}")
    private String host;
    /**
     * 代理端口
     */
    @Value("${gxd.sso.port:8080}")
    private int port;

    /**
     * cookie存活时间，默认一周
     */
    @Value("${gxd.sso.cookieMaxAge:604800}")
    private int cookieMaxAge;

    /**
     * 判断重复登录
     */
    @Value("${gxd.sso.repeatLogin:/sso/getTokenByStaffId/}")
    private String repeatLogin;

    /**
     * 获取所有权限信息
     */
    @Value("${gxd.sso.allAmpAuth:/sso/getAllAmpAuth}")
    private String allAmpAuth;
    /**
     * 根据用户信息获取权限信息
     */
    @Value("${gxd.sso.ampAuth:/sso/getAmpAuthByStaffAuthRel}")
    private String ampAuth;

    @Value("${gxd.sso.clientServerUrl:clientServerUrl}")
    private String clientServerUrl;

    /**
     * 系统前缀
     */
    @Value("${gxd.sso.systemPrefix:systemPrefix}")
    private String systemPrefix;

    /**
     * 网路变化验证
     */
    @Value("${gxd.sso.netWorkChange:false}")
    private Boolean netWorkChange;

    /**
     * 没有权限跳转地址
     */
    @Value("${gxd.sso.noAuth2Url:http://lbs.cindata.cn/gxdyun}")
    private String noAuth2Url;


    @Value("${gxd.sso.innerUrl:http://192.168.45.201:8099}")
    private String innerUrl;


    public String getInnerUrl() {
        return innerUrl;
    }

    public void setInnerUrl(String innerUrl) {
        this.innerUrl = innerUrl;
    }

    public String getNoAuth2Url() {
        return noAuth2Url;
    }

    public void setNoAuth2Url(String noAuth2Url) {
        this.noAuth2Url = noAuth2Url;
    }

    public String getRegUrl() {
        return regUrl;
    }

    public void setRegUrl(String regUrl) {
        this.regUrl = regUrl;
    }

    public String getSystemPrefix() {
        return systemPrefix;
    }

    public Boolean getNetWorkChange() {
        return netWorkChange;
    }

    public void setNetWorkChange(Boolean netWorkChange) {
        this.netWorkChange = netWorkChange;
    }

    public void setSystemPrefix(String systemPrefix) {
        this.systemPrefix = systemPrefix;
    }

    public String getClientServerUrl() {
        return clientServerUrl;
    }

    public void setClientServerUrl(String clientServerUrl) {
        this.clientServerUrl = clientServerUrl;
    }

    public String getSysRegisterToken() {
        return sysRegisterToken;
    }

    public void setSysRegisterToken(String sysRegisterToken) {
        this.sysRegisterToken = sysRegisterToken;
    }

    public String getAllAmpAuth() {
        return allAmpAuth;
    }

    public void setAllAmpAuth(String allAmpAuth) {
        this.allAmpAuth = allAmpAuth;
    }

    public String getAmpAuth() {
        return ampAuth;
    }

    public void setAmpAuth(String ampAuth) {
        this.ampAuth = ampAuth;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getRepeatLogin() {
        return repeatLogin;
    }

    public void setRepeatLogin(String repeatLogin) {
        this.repeatLogin = repeatLogin;
    }

    public int getCookieMaxAge() {
        return cookieMaxAge;
    }

    public void setCookieMaxAge(int cookieMaxAge) {
        this.cookieMaxAge = cookieMaxAge;
    }

    public Boolean getProxy() {
        return proxy;
    }

    public void setProxy(Boolean proxy) {
        this.proxy = proxy;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getServerDomain() {
        return serverDomain;
    }

    public void setServerDomain(String serverDomain) {
        this.serverDomain = serverDomain;
    }

    public String getServerLoginUrl() {
        return serverLoginUrl;
    }

    public void setServerLoginUrl(String serverLoginUrl) {
        this.serverLoginUrl = serverLoginUrl;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getSigningKey() {
        return signingKey;
    }

    public void setSigningKey(String signingKey) {
        this.signingKey = signingKey;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    @Override
    public String toString() {
        return String.format(
                "[CommConstConfig] serverDomain: %s, serverLoginUrl: %s, systemCode: %s, signingKey: %s,domainName: %s,adminId: %s",
                serverDomain, serverLoginUrl, systemCode, signingKey,domainName,adminId);
    }
}
