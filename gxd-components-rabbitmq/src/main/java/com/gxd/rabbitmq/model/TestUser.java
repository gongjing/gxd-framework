package com.gxd.rabbitmq.model;

import java.io.Serializable;

/**
 * @Author:gxd
 * @Description:
 * @Date: 16:56 2018/4/26
 * @Modified By:
 */
public class TestUser implements Serializable{

    private String id;
    private String userName;
    private int age;
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
