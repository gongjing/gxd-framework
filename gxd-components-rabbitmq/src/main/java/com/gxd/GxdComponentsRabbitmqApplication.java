package com.gxd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GxdComponentsRabbitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(GxdComponentsRabbitmqApplication.class, args);
	}
}
