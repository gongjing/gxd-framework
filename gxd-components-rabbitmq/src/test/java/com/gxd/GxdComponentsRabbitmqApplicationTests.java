package com.gxd;

import com.gxd.rabbitmq.Sender.RabbitMqSender;
import com.gxd.rabbitmq.model.TestUser;
import com.gxd.rabbitmq.utils.SerializeUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GxdComponentsRabbitmqApplicationTests {

	@Autowired
	RabbitMqSender rabbitMqSender;
	@Test
	public void contextLoads() {
	}

	@Test
	public void testSendRabbitmqDirect(){
		TestUser testUser = new TestUser();
		testUser.setAge(1);
		testUser.setEmail("1");
		testUser.setId("1");
		testUser.setId("1");
		testUser.setUserName("1");

		rabbitMqSender.sendRabbitmqDirect("TESTQUEUE1", SerializeUtil.serialize(testUser));
	}
	@Test
	public void testSendRabbitmqTopic(){
		TestUser testUser = new TestUser();
		testUser.setAge(1);
		testUser.setEmail("1");
		testUser.setId("1");
		testUser.setId("1");
		testUser.setUserName("1");

		rabbitMqSender.sendRabbitmqTopic("lazy.1.2",SerializeUtil.serialize(testUser));
	}
	@Test
	public void testSendRabbitmqTopic2(){
		TestUser testUser = new TestUser();
		testUser.setAge(1);
		testUser.setUserName("2");
		testUser.setEmail("1");
		testUser.setId("1");
		testUser.setId("1");
		rabbitMqSender.sendRabbitmqTopic("lazy.TEST.2",SerializeUtil.serialize(testUser));
	}

}
