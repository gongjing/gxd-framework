

package com.gxd.commons.entity;
import java.io.Serializable;

public class SmsLSMRequest implements Serializable {

    /**
     * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
     */
    private static final long serialVersionUID = 1L;

    /**
     * 螺丝猫apiKey
     */
    private String apiKey;

    /**
     * 螺丝猫调用地址
     */
    private String sendMsgUrl;

    /**
     * 发送手机号
     */
    private String phone;

    /**
     * 发送短信内容
     */
    private String code;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSendMsgUrl() {
        return sendMsgUrl;
    }

    public void setSendMsgUrl(String sendMsgUrl) {
        this.sendMsgUrl = sendMsgUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
