/*
* Project:kepler-tool
* Author:陈泉
* Company:中润普达
* Created Date:	2017年8月16日 下午5:59:55
* Copyright(c) @ 2017  
* @version V3.0
*/

package com.gxd.commons.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 发送邮件接口所需参数
 * 实例:SendMailRequest
 * 描述:TODO
 * 
 * 注意事项:
 *
 * 
 * @date 2017年8月21日 下午4:37:22
 * @author gxd
 */
public class SendMailRequest implements Serializable {

    /**
      * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
      */
    private static final long serialVersionUID = 1L;

    /**
     * 收件人
     */
    private String[] toUser;

    /**
     * 抄送人
     */
    private String[] ccUser;

    /**
     * 密送人
     */
    private String[] bccUser;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content = "";

    /**
     * 发送邮件文件对象接口所需参数
     */
    private List<SendMailFileRequest> sendMailFileRequestList;

    /**
     * 发件人
     */
    private String sendUser;

    /**
     * 发件人密码
     */
    private String sendPwd;

    public String[] getToUser() {
        return toUser;
    }

    public void setToUser(String[] toUser) {
        this.toUser = toUser;
    }

    public String[] getCcUser() {
        return ccUser;
    }

    public void setCcUser(String[] ccUser) {
        this.ccUser = ccUser;
    }

    public String[] getBccUser() {
        return bccUser;
    }

    public void setBccUser(String[] bccUser) {
        this.bccUser = bccUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<SendMailFileRequest> getSendMailFileRequestList() {
        return sendMailFileRequestList;
    }

    public void setSendMailFileRequestList(List<SendMailFileRequest> sendMailFileRequestList) {
        this.sendMailFileRequestList = sendMailFileRequestList;
    }

    public String getSendUser() {
        return sendUser;
    }

    public void setSendUser(String sendUser) {
        this.sendUser = sendUser;
    }

    public String getSendPwd() {
        return sendPwd;
    }

    public void setSendPwd(String sendPwd) {
        this.sendPwd = sendPwd;
    }

}
