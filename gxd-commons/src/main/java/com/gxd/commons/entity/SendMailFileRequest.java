

package com.gxd.commons.entity;

import java.io.Serializable;

/**
 * 发送邮件文件对象接口所需参数
 * 实例:SendMailRequest
 * 描述:TODO
 * 
 * 注意事项:
 *
 * 
 * @date 2017年8月21日 下午4:37:22
 * @author gxd
 */
public class SendMailFileRequest implements Serializable {

    /**
      * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
      */
    private static final long serialVersionUID = 1L;

    /**
     * 附件地址
     */
    private String filePaths;

    /**
     * 附件名字
     */
    private String showNames;

    public String getFilePaths() {
        return filePaths;
    }

    public void setFilePaths(String filePaths) {
        this.filePaths = filePaths;
    }

    public String getShowNames() {
        return showNames;
    }

    public void setShowNames(String showNames) {
        this.showNames = showNames;
    }

}
