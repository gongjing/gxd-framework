

package com.gxd.commons.entity;

import java.io.Serializable;
import java.util.Map;

public class SmsRequest implements Serializable {

    /**
     * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
     */
    private static final long serialVersionUID = 1L;

    /**
     * 阿里大于AppId
     */
	private String phoneNum;


    /**
     * 短信内容
     */
	private Map data;

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public Map getData() {
		return data;
	}

	public void setData(Map data) {
		this.data = data;
	}
}
