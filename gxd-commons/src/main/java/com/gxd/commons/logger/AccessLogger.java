package com.gxd.commons.logger;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.appender.rolling.CompositeTriggeringPolicy;
import org.apache.logging.log4j.core.appender.rolling.SizeBasedTriggeringPolicy;
import org.apache.logging.log4j.core.appender.rolling.TimeBasedTriggeringPolicy;
import org.apache.logging.log4j.core.appender.rolling.TriggeringPolicy;
import org.apache.logging.log4j.core.async.AsyncLoggerConfig;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 访问日志记录对象
 */
public class AccessLogger {

    private static final Logger logger = LoggerFactory.getLogger(AccessLogger.class);
    private static final String logDir = "/app/logs/";
    private static final String logName = "accesslog";
    private static org.apache.logging.log4j.Logger accesslogger = null;

    static {
        init();
    }

    private static void init(){
        try {
            LoggerContext ctx = (LoggerContext)LogManager.getContext(false);
            Configuration config = ctx.getConfiguration();
            Layout layout = PatternLayout.newBuilder().withConfiguration(config).withPattern("%msg%n").build();
            TimeBasedTriggeringPolicy tbtp = TimeBasedTriggeringPolicy.createPolicy((String)null, (String)null);
            TriggeringPolicy tp = SizeBasedTriggeringPolicy.createPolicy("500M");
            CompositeTriggeringPolicy policyComposite = CompositeTriggeringPolicy.createPolicy(new TriggeringPolicy[]{tbtp, tp});
            RollingFileAppender appender = ((RollingFileAppender.Builder)((RollingFileAppender.Builder)RollingFileAppender.newBuilder().withFileName("/app/logs/accesslog.log").withFilePattern("/app/logs/accesslog.%d{yyyy-MM-dd}.%i.log").withAppend(true).withName("accesslog")).withPolicy(policyComposite).withLayout(layout)).withConfiguration(config).build();
            appender.start();
            config.addAppender(appender);
            AppenderRef ref = AppenderRef.createAppenderRef("accesslog", (Level)null, (Filter)null);
            AppenderRef[] refs = new AppenderRef[]{ref};
            LoggerConfig loggerConfig = AsyncLoggerConfig.createLogger(false, Level.INFO, "accesslog", "true", refs, (Property[])null, config, (Filter)null);
            loggerConfig.addAppender(appender, (Level)null, (Filter)null);
            config.addLogger("accesslog", loggerConfig);
            ctx.updateLoggers();
            accesslogger = LogManager.getLogger("accesslog");
        } catch (Exception var10) {
            logger.error(" AccessLogger init Exception ", var10);
        }
    }

    /**
     * 记录日志
     * @param msg
     */
    public static void info(String msg){
        if(accesslogger != null){
            accesslogger.info(msg);
        }
    }


}
