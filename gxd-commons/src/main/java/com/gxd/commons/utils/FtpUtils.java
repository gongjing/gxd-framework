package com.gxd.commons.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.pool2.ObjectPool;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
/**
 * @Author:gxd
 * @Description: Ftp工具类
 * @Date: 10:25 2018/3/27
 * @Modified By:
 */
public class FtpUtils {
    protected final static Logger log = LoggerFactory.getLogger(FtpUtils.class);

    public static String LOCAL_CHARSET="UTF-8";
    public static String DEFAULT_CHARSET="iso-8859-1";
    /**
     * ftpClient连接池初始化标志
     */
    private static volatile boolean hasInit = false;
    /**
     * ftpClient连接池
     */
    private static ObjectPool<FTPClient> ftpClientPool;

    /**
     * 初始化ftpClientPool
     *
     * @param ftpClientPool
     */
    public static void init(ObjectPool<FTPClient> ftpClientPool) {
        if (!hasInit) {
            synchronized (FtpUtils.class) {
                if (!hasInit) {
                    FtpUtils.ftpClientPool = ftpClientPool;
                    hasInit = true;
                }
            }
        }
    }
    /**
     * 读取csv文件
     *
     * @param remoteFilePath 文件路径（path+fileName）
     * @param headers 列头
     * @return
     * @throws IOException
     */
    public static List<CSVRecord> readCsvFile(String remoteFilePath, String... headers) throws Exception {
        FTPClient ftpClient = getFtpClient(remoteFilePath);
        try (InputStream in = ftpClient.retrieveFileStream(encodingPath(remoteFilePath))) {
            return CSVFormat.EXCEL.withHeader(headers).withSkipHeaderRecord(false)
                    .withIgnoreSurroundingSpaces().withIgnoreEmptyLines()
                    .parse(new InputStreamReader(in, "utf-8")).getRecords();
        } finally {
            ftpClient.completePendingCommand();
            releaseFtpClient(ftpClient);
        }
    }


    /**
     * 按行读取FTP文件
     *
     * @param remoteFilePath 文件路径（path+fileName）
     * @return
     * @throws IOException
     */
    public static List<String> readFileByLine(String remoteFilePath) throws Exception {
        FTPClient ftpClient = getFtpClient(remoteFilePath);
        try (InputStream in = ftpClient.retrieveFileStream(encodingPath(remoteFilePath));
             BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
            return br.lines().map(line -> StringUtils.trimToEmpty(line))
                    .filter(line -> StringUtils.isNotEmpty(line)).collect(Collectors.toList());
        } finally {
            ftpClient.completePendingCommand();
            releaseFtpClient(ftpClient);
        }
    }

    /**
     * 获取指定路径下FTP文件
     *
     * @param remoteFilePath 路径
     * @return FTPFile数组
     * @throws IOException
     */
    public static FTPFile[] retrieveFTPFiles(String remoteFilePath) throws Exception {
        FTPClient ftpClient = getFtpClient(remoteFilePath);
        try {
            return ftpClient.listFiles(encodingPath(remoteFilePath + "/"),
                    file -> file != null && file.getSize() > 0);
        } finally {
            releaseFtpClient(ftpClient);
        }
    }

    /**
     * 获取指定路径下FTP文件名称
     *
     * @param remotePath 路径
     * @return ftp文件名称列表
     * @throws IOException
     */
    public static List<String> retrieveFileNames(String remotePath) throws Exception {
        FTPFile[] ftpFiles = retrieveFTPFiles(remotePath);
        if (ArrayUtils.isEmpty(ftpFiles)) {
            return new ArrayList<>();
        }
        return Arrays.stream(ftpFiles).filter(Objects::nonNull)
                .map(FTPFile::getName).collect(Collectors.toList());
    }

    /**
     * 编码文件路径
     */
    public static String encodingPath(String path) throws UnsupportedEncodingException {
        // FTP协议里面，规定文件名编码为iso-8859-1，所以目录名或文件名需要转码
        String codePath = new String(path.replaceAll("//", "/").getBytes("UTF-8"), "iso-8859-1");
        log.debug("图片编码后路径："+codePath);
        return codePath;
    }


    /**
     * 获取ftpClient
     *
     * @return
     */
    public static FTPClient getFtpClient(String remoteFilePath) throws Exception{
        checkFtpClientPoolAvailable();
        FTPClient ftpClient = null;
        Exception ex = null;
        String workingDirectory ="";
        if (remoteFilePath.contains("/")) {
            workingDirectory = remoteFilePath.substring(0,remoteFilePath.lastIndexOf("/"));
        }else{
            throw new Exception("图片路径不对，路径需包含/,从根目录开始，如/picture/2017/test.jpg");
        }
        // 获取连接最多尝试3次
        for (int i = 0; i < 3; i++) {
            try {
                ftpClient = ftpClientPool.borrowObject();
                //防止中文编码导致目录找不到
                String directory  = new String(workingDirectory.getBytes("UTF-8"),DEFAULT_CHARSET);
                //更换目录到当前目录
                long startTime = System.currentTimeMillis();
                boolean b = ftpClient.changeWorkingDirectory(directory);
                log.debug("切换路径："+workingDirectory+":"+b);
                log.debug("切换路径 SPEND TIME : " + StringUtils.formatDuring(System.currentTimeMillis() - startTime));
                break;
            } catch (Exception e) {
                ex = e;
            }
        }
        if (ftpClient == null) {
            throw new RuntimeException("Could not get a ftpClient from the pool", ex);
        }
        return ftpClient;
    }

    /**
     * 释放ftpClient
     */
    public static void releaseFtpClient(FTPClient ftpClient) {
        if (ftpClient == null) {
            return;
        }
        try {
            ftpClientPool.returnObject(ftpClient);
        } catch (Exception e) {
            log.error("Could not return the ftpClient to the pool", e);
            // destoryFtpClient
            if (ftpClient.isAvailable()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException io) {
                }
            }
        }
    }

    /**
     * 检查ftpClientPool是否可用
     */
    private static void checkFtpClientPoolAvailable() {
        Assert.state(hasInit, "FTP未启用或连接失败！");
    }

    /**
     * 上传Excel文件到FTP
     * @param workbook
     * @param remoteFilePath
     * @throws IOException
     */
    public static boolean uploadExcel2Ftp(Workbook workbook, String remoteFilePath) throws Exception {
        Assert.notNull(workbook, "workbook cannot be null.");
        Assert.hasText(remoteFilePath, "remoteFilePath cannot be null or blank.");
        FTPClient ftpClient = getFtpClient(remoteFilePath);
        try (OutputStream out = ftpClient.storeFileStream(encodingPath(remoteFilePath))) {
            workbook.write(out);
            workbook.close();
            return true;
        } finally {
            ftpClient.completePendingCommand();
            releaseFtpClient(ftpClient);
        }
    }

    /**
     * 从ftp下载excel文件
     * @param remoteFilePath
     * @param response
     * @throws IOException
     */
    public static void downloadExcel(String remoteFilePath, HttpServletResponse response)
            throws Exception {
        String fileName = remoteFilePath.substring(remoteFilePath.lastIndexOf("/") + 1);
        fileName = new String(fileName.getBytes("UTF-8"), "iso-8859-1");
        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName);

        FTPClient ftpClient = getFtpClient(remoteFilePath);
        try (InputStream in = ftpClient.retrieveFileStream(encodingPath(remoteFilePath));
             OutputStream out = response.getOutputStream()) {
            int size = 0;
            byte[] buf = new byte[10240];
            while ((size = in.read(buf)) > 0) {
                out.write(buf, 0, size);
                out.flush();
            }
        } finally {
            ftpClient.completePendingCommand();
            releaseFtpClient(ftpClient);
        }
    }
}
