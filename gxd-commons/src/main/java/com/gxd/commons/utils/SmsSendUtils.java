

package com.gxd.commons.utils;

import com.aliyuncs.exceptions.ClientException;

import java.util.HashMap;
import java.util.Map;

public class SmsSendUtils {


	private static final PropertiesUtil util = new PropertiesUtil("sms.properties");
	//private static final String product = "Dysmsapi";
	//private static final String domain = "dysmsapi.aliyuncs.com";
	private static final String accessKeyId = util.readProperty("ACCESS_KEY_ID");
	private static final String signName = util.readProperty("ACCESS_SIGN_NAME");
	private static final String URL = util.readProperty("URL");

	/**
	 *	云片短信
	 * @param code
	 * @return
	 * @throws ClientException
	 */
	public static String send(String code,String phone) throws ClientException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("apikey", accessKeyId);
		params.put("text", signName.replace("code",code));
		params.put("mobile", phone);
		String result = HttpUtils.post(URL, params);
		return result;
		/*String phoneNum = smsRequest.getPhoneNum();
		Map data = smsRequest.getData();
		String jsonData = NetJsonUtils.bean2json(data);
		return sendSms(phoneNum, signName, templateCode, jsonData);*/
	}
	/**
	 *	云片短信
	 * @param code
	 * @return
	 * @throws ClientException
	 */
	public static String send(String code,String phone,String sytemName) throws ClientException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("apikey", accessKeyId);
		params.put("text", signName.replace("code",code).replace("国信达云",sytemName));
		params.put("mobile", phone);
		String result = HttpUtils.post(URL, params);
		return result;
		/*String phoneNum = smsRequest.getPhoneNum();
		Map data = smsRequest.getData();
		String jsonData = NetJsonUtils.bean2json(data);
		return sendSms(phoneNum, signName, templateCode, jsonData);*/
	}

	/**
	 * 阿里发送短信（验证码）
	 *
	 * @return int
	 */
	/*private  static SendSmsResponse sendSms(String phoneNum, String signName, String templateCode, String json) throws ClientException{
		System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
		System.setProperty("sun.net.client.defaultReadTimeout", "10000");

		IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
		DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "Dysmsapi", "dysmsapi.aliyuncs.com");
		IAcsClient acsClient = new DefaultAcsClient(profile);

		SendSmsRequest request = new SendSmsRequest();

		request.setPhoneNumbers(phoneNum);

		request.setSignName(signName);

		request.setTemplateCode(templateCode);

		request.setTemplateParam(json);

		SendSmsResponse sendSmsResponse = (SendSmsResponse)acsClient.getAcsResponse(request);

		return sendSmsResponse;
	}*/



	public static void main(String[] args) throws ClientException, InterruptedException
	{

	}

}
