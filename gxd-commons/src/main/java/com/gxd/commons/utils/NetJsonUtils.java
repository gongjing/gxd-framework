package com.gxd.commons.utils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;
import net.sf.json.util.PropertyFilter;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


public final class NetJsonUtils
{
    public static String array2json(Object object)
    {
        JSONArray jsonArray = JSONArray.fromObject(object);
        return jsonArray.toString();
    }

    public static Object json2Array(String json, Class valueClz)
    {
        JSONArray jsonArray = JSONArray.fromObject(json);
        return JSONArray.toArray(jsonArray, valueClz);
    }

    public static Object json2List(String json, Class valueClz)
    {
        JSONArray jsonArray = JSONArray.fromObject(json);
        return JSONArray.toList(jsonArray, valueClz);
    }

    public static String collection2json(Object object)
    {
        JsonConfig config = new JsonConfig();
        config.setJsonPropertyFilter(new PropertyFilter()
        {
            public boolean apply(Object source, String name, Object value) {
                return ((name.equals("city")) || (name.equals("district")) || (name.equals("area")));
            }

        });
        JSONArray jsonArray = JSONArray.fromObject(object, config);
        return jsonArray.toString();
    }

    public static String map2json(Object object)
    {
        JSONObject jsonObject = JSONObject.fromObject(object);
        return jsonObject.toString();
    }

    public static Map json2Map(Object[] keyArray, String json, Class valueClz)
    {
        JSONObject jsonObject = JSONObject.fromObject(json);
        Map classMap = new HashMap();

        for (int i = 0; i < keyArray.length; ++i) {
            classMap.put(keyArray[i], valueClz);
        }

        return ((Map)JSONObject.toBean(jsonObject, Map.class, classMap));
    }

    public static String bean2json(Object object)
    {
        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.registerJsonValueProcessor(Date.class, new JsonValueProcessor() {
            private String format;

            public Object processArrayValue(Object paramObject, JsonConfig paramJsonConfig) { return process(paramObject); }

            public Object processObjectValue(String paramString, Object paramObject, JsonConfig paramJsonConfig) { return process(paramObject);
            }

            private Object process(Object value) {
                if (value instanceof Date) {
                    SimpleDateFormat sdf = new SimpleDateFormat(this.format, Locale.CHINA);
                    return sdf.format(value);
                }
                return ((value == null) ? "" : value.toString());
            }

        });
        JSONObject jsonObject = JSONObject.fromObject(object, jsonConfig);
        return jsonObject.toString();
    }

    public static Object json2Object(String json, Class beanClz)
    {
        return JSONObject.toBean(JSONObject.fromObject(json), beanClz);
    }

    public static Object json2Object(String json, Class beanClz, Map<String, Class> classMap) {
        return JSONObject.toBean(JSONObject.fromObject(json), beanClz, classMap);
    }

    public static <T> T fromJsonToObject(String json, Class<T> valueType)
    {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(json, valueType);
        } catch (JsonParseException e) {
        } catch (JsonMappingException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static String string2json(String key, String value)
    {
        JSONObject object = new JSONObject();
        object.put(key, value);
        return object.toString();
    }

    public static String json2String(String json, String key)
    {
        JSONObject jsonObject = JSONObject.fromObject(json);
        return jsonObject.get(key).toString();
    }

    public static <T> String toJSONString(List<T> list)
    {
        JSONArray jsonArray = JSONArray.fromObject(list);

        return jsonArray.toString();
    }

    public static String toJSONString(Object object)
    {
        JSONArray jsonArray = JSONArray.fromObject(object);

        return jsonArray.toString();
    }

    public static String toJSONString(JSONArray jsonArray)
    {
        return jsonArray.toString();
    }

    public static String toJSONString(JSONObject jsonObject)
    {
        return jsonObject.toString();
    }

    public static List toArrayList(Object object)
    {
        List arrayList = new ArrayList();

        JSONArray jsonArray = JSONArray.fromObject(object);

        Iterator it = jsonArray.iterator();
        while (it.hasNext()) {
            JSONObject jsonObject = (JSONObject)it.next();

            Iterator keys = jsonObject.keys();
            while (keys.hasNext()) {
                Object key = keys.next();
                Object value = jsonObject.get(key);
                arrayList.add(value);
            }
        }

        return arrayList;
    }

    public static JSONArray toJSONArray(Object object)
    {
        return JSONArray.fromObject(object);
    }

    public static JSONObject toJSONObject(Object object)
    {
        return JSONObject.fromObject(object);
    }

    public static HashMap toHashMap(Object object)
    {
        HashMap data = new HashMap();
        JSONObject jsonObject = toJSONObject(object);
        Iterator it = jsonObject.keys();
        while (it.hasNext()) {
            String key = String.valueOf(it.next());
            Object value = jsonObject.get(key);
            data.put(key, value);
        }

        return data;
    }

    public static List<Map<String, Object>> toList(Object object)
    {
        List list = new ArrayList();
        JSONArray jsonArray = JSONArray.fromObject(object);
        for (Iterator i$ = jsonArray.iterator(); i$.hasNext(); ) { Object obj = i$.next();
            JSONObject jsonObject = (JSONObject)obj;
            Map map = new HashMap();
            Iterator it = jsonObject.keys();
            while (it.hasNext()) {
                String key = (String)it.next();
                Object value = jsonObject.get(key);
                map.put(key, value);
            }
            list.add(map);
        }
        return list;
    }

    public static <T> List<T> toList(JSONArray jsonArray, Class<T> objectClass)
    {
        return JSONArray.toList(jsonArray, objectClass);
    }

    public static <T> List<T> toList(Object object, Class<T> objectClass)
    {
        JSONArray jsonArray = JSONArray.fromObject(object);

        return JSONArray.toList(jsonArray, objectClass);
    }

}