package com.gxd.commons.utils;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class JwtUtils {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public static String generateToken(String signingKey, String subject) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        JwtBuilder builder = Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(now)//是一个时间戳，代表这个JWT的签发时间；
                .signWith(SignatureAlgorithm.HS256, signingKey);
        String token = builder.compact();
        return token;
    }

    public static String parseToken(String token, String signingKey){
        String subject = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token).getBody().getSubject();
        return subject;
    }
}

