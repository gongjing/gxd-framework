package com.gxd.commons.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegXUtils {

    public static boolean ifMatch(String src, String regx) {
        String str = src;
        // 邮箱验证规则
        String regEx = regx;
        // 编译正则表达式
        Pattern pattern = Pattern.compile(regEx);
        // 忽略大小写的写法
        Matcher matcher = pattern.matcher(str);
        // 字符串是否与正则表达式相匹配
        boolean rs = matcher.matches();

        return rs;
    }
    /**
     * 车牌号码Pattern
     */
    public static final Pattern PLATE_NUMBER_PATTERN = Pattern
            .compile("^[\u0391-\uFFE5]{1}[a-zA-Z0-9]{6}$");

    /**
     * 证件号码Pattern
     */
    public static final Pattern ID_CODE_PATTERN = Pattern
            .compile("^[a-zA-Z0-9]+$");

    /**
     * 编码Pattern
     */
    public static final Pattern CODE_PATTERN = Pattern
            .compile("^[a-zA-Z0-9]+$");

    /**
     * 固定电话编码Pattern
     */
    public static final Pattern PHONE_NUMBER_PATTERN = Pattern
            .compile("0\\d{2,3}-[0-9]+");

    /**
     * 邮政编码Pattern
     */
    public static final Pattern POST_CODE_PATTERN = Pattern.compile("\\d{6}");

    /**
     * 面积Pattern
     */
    public static final Pattern AREA_PATTERN = Pattern.compile("\\d*.?\\d*");

    /**
     * 手机号码Pattern
     */
    public static final Pattern MOBILE_NUMBER_PATTERN = Pattern
            .compile("\\d{11}");

    /**
     * 银行帐号Pattern
     */
    public static final Pattern ACCOUNT_NUMBER_PATTERN = Pattern
            .compile("\\d{16,21}");


    /**
     * 正浮点数Pattern
     */
    public static final Pattern POSITIVE_FLOATING_PATTERN = Pattern
            .compile( "^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$");

    /**
     * 车牌号码是否正确
     *
     * @param s
     * @return
     */
    public static boolean isPlateNumber(String s) {
        Matcher m = PLATE_NUMBER_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 证件号码是否正确
     *
     * @param s
     * @return
     */
    public static boolean isIDCode(String s) {
        Matcher m = ID_CODE_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 编码是否正确
     *
     * @param s
     * @return
     */
    public static boolean isCode(String s) {
        Matcher m = CODE_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 固话编码是否正确
     *
     * @param s
     * @return
     */
    public static boolean isPhoneNumber(String s) {
        Matcher m = PHONE_NUMBER_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 邮政编码是否正确
     *
     * @param s
     * @return
     */
    public static boolean isPostCode(String s) {
        Matcher m = POST_CODE_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 面积是否正确
     *
     * @param s
     * @return
     */
    public static boolean isArea(String s) {
        Matcher m = AREA_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 手机号码否正确
     *
     * @param s
     * @return
     */
    public static boolean isMobileNumber(String s) {
        Matcher m = MOBILE_NUMBER_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 银行账号否正确
     *
     * @param s
     * @return
     */
    public static boolean isAccountNumber(String s) {
        Matcher m = ACCOUNT_NUMBER_PATTERN.matcher(s);
        return m.matches();
    }

    /**
     * 验证正浮点数是否正确
     * @param s
     * @return
     */
    public  static boolean isPositiveFloating(String s){
        Matcher m = POSITIVE_FLOATING_PATTERN.matcher(s);
        return m.matches();
    }

    public  static boolean isTrue(Pattern pattern ,String s){
        Matcher m = pattern.matcher(s);
        return m.matches();
    }
    public static void main(String[] args) {
//        Pattern ACCOUNT_NUMBER_PATTERN = Pattern
//                .compile("^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$");
//        System.out.println("0.1: "+isTrue(ACCOUNT_NUMBER_PATTERN,"0.1"));
//        System.out.println("0: "+isTrue(ACCOUNT_NUMBER_PATTERN,"0"));
//        System.out.println("-0: "+isTrue(ACCOUNT_NUMBER_PATTERN,"-0"));
//        System.out.println("-0.1: "+isTrue(ACCOUNT_NUMBER_PATTERN,"-0.1"));
//        System.out.println("1: "+isTrue(ACCOUNT_NUMBER_PATTERN,"1"));
//        System.out.println("-1: "+isTrue(ACCOUNT_NUMBER_PATTERN,"-1"));
//        System.out.println("-1.1: "+isTrue(ACCOUNT_NUMBER_PATTERN,"-1.1"));
//        System.out.println("-1fdsaf: "+isTrue(ACCOUNT_NUMBER_PATTERN,"-1fdsaf"));
//        System.out.println("fadsf: "+isTrue(ACCOUNT_NUMBER_PATTERN,"fadsf"));

//        String s = "(\\>|\\>=|\\=|\\<|\\<=|\\!=){1}(([1-9]+\\d*\\.*\\d*))$";
//        Pattern pattern = Pattern
//                .compile(s);
//
//        System.out.println(isTrue(pattern,">111.2"));
    }
}
