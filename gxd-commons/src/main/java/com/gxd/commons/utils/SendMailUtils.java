

package com.gxd.commons.utils;

import com.gxd.commons.constants.ExceptionConstant;
import com.gxd.commons.entity.SendMailFileRequest;
import com.gxd.commons.entity.SendMailRequest;
import com.gxd.commons.exception.ToolException;

import java.util.List;


public class SendMailUtils {

    /**
     * 发送邮件
     */
    public void sendMail(SendMailRequest request) throws Exception {

        String[] toUser = request.getToUser();

        String[] ccUser = request.getCcUser();

        String[] bccUser = request.getBccUser();

        String title = request.getTitle();

        String content = request.getContent() == null ? "" : request.getContent();

        List<SendMailFileRequest> sendMailFileRequestList = request.getSendMailFileRequestList();

        String sendUser = request.getSendUser();

        String sendPwd = request.getSendPwd();

        if (StringUtils.isBlank(sendUser)) {
            throw new ToolException(ExceptionConstant.NO_SEND_USER.getValue(), ExceptionConstant.NO_SEND_USER.getMsg());
        }
        if (StringUtils.isBlank(sendPwd)) {
            throw new ToolException(ExceptionConstant.NO_SEND_PWD.getValue(), ExceptionConstant.NO_SEND_PWD.getMsg());
        }
        // 发送邮件开始
        SendmailUtil.sendMail(toUser, ccUser, bccUser, sendUser, sendPwd, title, content, sendMailFileRequestList);

    }

    public static void main(String[] args) throws Exception{
        String[] toUser={"648226488@qq.com"};//抄送人
        String[] ccUser={"648226488@qq.com"};
        String[] bccUser={"648226488@qq.com"};
        String sendUser ="dataqu@bigdatadh.com";////发送人
        String sendPwd = "Abc12345";
        String title = "123455";
        String content = "测试内容";
        List<SendMailFileRequest> sendMailFileRequestList= null;
        SendmailUtil.sendMail(toUser, ccUser, bccUser, sendUser, sendPwd, title, content, sendMailFileRequestList);

    }
}
