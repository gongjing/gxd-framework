package com.gxd.commons.utils;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Type;
import java.util.Properties;

public class PropertiesUtil {
    protected final static Logger log = LoggerFactory.getLogger(PropertiesUtil.class);

    private String properiesName = "";

    public PropertiesUtil() {
    }

    public PropertiesUtil(String fileName) {
        this.properiesName = fileName;
    }

    public String readProperty(String key)
    {
        String value = "";
        InputStream is = null;
        try {
            is = PropertiesUtil.class.getClassLoader().getResourceAsStream(this.properiesName);

            Properties p = new Properties();
            p.load(new InputStreamReader(is,"UTF-8"));
            value = p.getProperty(key);
        }
        catch (IOException e) {
            log.error("IO异常信息："+e.getMessage());
        } finally {
            try {
                is.close();
            }
            catch (IOException e) {
                log.error("IO异常信息："+e.getMessage());
            }
        }
        return value;
    }

    public Properties getProperties() {
        Properties p = new Properties();
        InputStream is = null;
        try {
            is = PropertiesUtil.class.getClassLoader().getResourceAsStream(this.properiesName);

            p.load(new InputStreamReader(is,"UTF-8"));
        }
        catch (IOException e) {
            log.error("IO异常信息："+e.getMessage());
        } finally {
            try {
                is.close();
            }
            catch (IOException e) {
                log.error("IO异常信息："+e.getMessage());
            }
        }
        return p;
    }

    public void writeProperty(String key, String value) {
        InputStream is = null;
        OutputStream os = null;
        Properties p = new Properties();
        try {
            is = new FileInputStream(this.properiesName);
            p.load(is);
            os = new FileOutputStream(PropertiesUtil.class.getClassLoader().getResource(this.properiesName).getFile());

            p.setProperty(key, value);
            p.store(os, key);
            os.flush();
            os.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != is) {
                    is.close();
                }
                if (null != os) {
                    os.close();
                }
            }
            catch (IOException e) {
                log.error("IO异常信息："+e.getMessage());
            }
        }
    }

    public static <T> void buildUpdateEntity(T oentity, T uentity)
    {
        PropertyDescriptor[] origDescriptors = PropertyUtils.getPropertyDescriptors(oentity);
        try
        {
            for (int i = 0; i < origDescriptors.length; ++i) {
                String name = origDescriptors[i].getName();
                Type type = origDescriptors[i].getPropertyType();
                if (name.equals("class")) {
                    continue;
                }

                Object uvalue = PropertyUtils.getSimpleProperty(uentity, name);
                if ((uvalue != null) && (!("".equals(uvalue)))) {
                    PropertyUtils.setSimpleProperty(oentity, name, uvalue);
                }
            }
        }
        catch (Exception e) {
            log.error("异常信息："+e.getMessage());
        }
    }
}
