package com.gxd.commons.utils;

import com.gxd.commons.enums.ResultCodeEnum;
import com.gxd.commons.model.CommonResult;

/**
 * @Author:gxd
 * @Description:公共响应结果成功失败的静态方法调用
 * @Date: 16:59 2018/7/9
 * @Modified By:
 */
public class ResultUtils {
    /**
     * return success
     *
     * @param data
     * @return
     */
    public static <T> CommonResult<T> returnSuccess(T data) {
        CommonResult<T> result = new CommonResult();
        result.setStatus(CommonResult.SUCCESS);
        result.setSuccess(true);
        result.setData(data);
        result.setInfo(CommonResult.msg);
        return result;
    }

    /**
     * return success
     * @param data
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> CommonResult<T> returnSuccess(T data,String msg) {
        CommonResult<T> result = new CommonResult();
        result.setStatus(CommonResult.SUCCESS);
        result.setSuccess(true);
        result.setData(data);
        result.setInfo(msg);
        return result;
    }

    /**
     * return error
     *
     * @param status error status
     * @param info  error info
     * @return
     */
    public static CommonResult returnError(int status, String info) {
        CommonResult result = new CommonResult();
        result.setStatus(status);
        result.setData("");
        result.setInfo(info);
        return result;
    }
    /**
     * use enum
     *
     * @param status
     * @return
     */
    public static CommonResult returnError(ResultCodeEnum status) {
        return returnError(status.getStatus(), status.getDesc());
    }

}
