package com.gxd.commons.model;

import java.io.Serializable;

/**
 * @Author:gxd
 * @Description:
 * @Date: 16:57 2018/7/9
 * @Modified By:
 */
public class CommonResult <T> implements Serializable {

    /**
     * serialVersionUID:.
     */
    private static final long serialVersionUID = -7268040542410707954L;

    public static final int SUCCESS = 0;

    public static final int FAIL = -1;

    public static String msg = "success";
    /**
     * 是否成功
     */
    public boolean success = false;

    /**
     * 装载数据
     */
    private T data;

    /**
     *状态码(接口状态)
     */
    private int status = SUCCESS;

    /**
     * 提示信息
     */
    private  String info = "success";

    /**
     * 总记录数
     */
    private Integer total = 0;

    /**
     * 总页数
     */
    private Integer pageCount=0;

    /**
     * 默认构造器
     */
    public CommonResult() {

    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    /**
     * @param success 是否成功
     * @param info 返回的消息
     */
    public CommonResult(boolean success, String info) {
        this.success = success;
        this.info = info;
    }

    /**
     * @param success 是否成功
     */
    public CommonResult(boolean success) {
        this.success = success;
    }

    /**
     * @param status    error code
     * @param info success or error info
     */
    public CommonResult(int status, String info) {
        this.status = status;
        this.info = info;
    }

    /**
     * @param success 是否成功
     * @param info 消息
     * @param data    数据
     */
    public CommonResult(boolean success, String info, T data) {
        this.success = success;
        this.info = info;
        this.data = data;
    }

    /**
     *
     * @param success 是否成功
     * @param status 返回码
     * @param info 消息
     * @param data 数据
     */
    public CommonResult(boolean success,int status, String info, T data) {
        this.success = success;
        this.info = info;
        this.data = data;
        this.status = status;
    }

    /**
     *
     * @param status 返回码
     * @param info 消息
     * @param data 数据
     */
    public CommonResult(int status, String info, T data) {
        this.info = info;
        this.data = data;
        this.status = status;
    }
    /**
     *
     * @param success 是否成功
     * @param info 消息
     * @param data
     * @param total 数据
     */
    public CommonResult(boolean success, String info, T data,Integer total) {
        this.success = success;
        this.info = info;
        this.data = data;
        this.total = total;
    }
    /**
     *
     * @param success 是否成功
     * @param info 消息
     * @param data
     * @param total 数据
     */
    public CommonResult(boolean success, int status,String info, T data,Integer total) {
        this.status = status;
        this.success = success;
        this.info = info;
        this.data = data;
        this.total = total;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
