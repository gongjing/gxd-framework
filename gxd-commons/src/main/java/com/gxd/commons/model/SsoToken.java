package com.gxd.commons.model;

import java.io.Serializable;
import java.util.List;

/**
 * 用户token信息
 * @author gxd
 * @date 2018/2/25
 */
public class SsoToken implements Serializable  {

    private static final long serialVersionUID = -3543201508657923400L;
    private Long staffId;
    private String loginName;
    private String staffCode;
    private String staffName;
    private String telephone;
    private String systemToken;
    private String sysRegisterToken;
    private String loginTime;
    private String ip;
    private List<String> systemUrl;
    private List<String> systemCode;
    private List<String> rolesId;

    public List<String> getSystemUrl() {
        return systemUrl;
    }

    public void setSystemUrl(List<String> systemUrl) {
        this.systemUrl = systemUrl;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getStaffCode() {
        return staffCode;
    }

    public void setStaffCode(String staffCode) {
        this.staffCode = staffCode;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSystemToken() {
        return systemToken;
    }

    public void setSystemToken(String systemToken) {
        this.systemToken = systemToken;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public List<String> getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(List<String> systemCode) {
        this.systemCode = systemCode;
    }

    public List<String> getRolesId() {
        return rolesId;
    }

    public void setRolesId(List<String> rolesId) {
        this.rolesId = rolesId;
    }

    public String getSysRegisterToken() {
        return sysRegisterToken;
    }

    public void setSysRegisterToken(String sysRegisterToken) {
        this.sysRegisterToken = sysRegisterToken;
    }
}
