package com.gxd.commons.base;

import com.gxd.commons.enums.ResultCodeEnum;
import com.gxd.commons.model.CommonResult;
import com.gxd.commons.utils.ResultUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @Author:gxd
 * @Description:
 * @Date: 17:03 2018/7/9
 * @Modified By:
 */
public abstract class BaseController {
    public static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpSession session;

    @ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
        this.session = request.getSession();
    }

    /**
     * validate params
     *
     * @param bindingResult
     * @return
     */
    protected CommonResult validParams(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            return processBindingError(fieldError);
        }
        return ResultUtils.returnSuccess("");
    }

    /**
     * 根据spring binding 错误信息自定义返回错误码和错误信息
     *
     * @param fieldError
     * @return
     */
    private CommonResult processBindingError(FieldError fieldError) {
        String code = fieldError.getCode();
        LOGGER.debug("validator error code: {}", code);
        switch (code) {
            case "NotEmpty":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_EMPTY.getStatus(), fieldError.getDefaultMessage());
            case "NotBlank":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_EMPTY.getStatus(), fieldError.getDefaultMessage());
            case "NotNull":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_EMPTY.getStatus(), fieldError.getDefaultMessage());
            case "Pattern":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Min":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Max":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Length":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Range":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Email":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "DecimalMin":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "DecimalMax":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Size":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Digits":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Past":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            case "Future":
                return ResultUtils.returnError(ResultCodeEnum.PARAM_ERROR.getStatus(), fieldError.getDefaultMessage());
            default:
                return ResultUtils.returnError(ResultCodeEnum.UNKNOWN_ERROR);
        }
    }
}
