package com.gxd.commons.constants;

/**
 * @Author:gxd
 * @Description:
 * @Date: 19:45 2018/3/14
 * @Modified By:
 */
public class RedisConstant {
    public static String REDIS_LOGIN_COUNT = "sso:login:count:";

    public static String REDIS_LOGIN_LOCK = "sso:login:lock:";

    public static String REDIS_SSO_TOKEN = "sso:token:";

    public static String REDIS_SSO_CODE = "sso:code:";

    public static String REDIS_SSO_LOGIN_ELSE_WHERE = "sso:login:elsewhere";

    public static String REDIS_SSO_ENCRYPT_TOKEN = "sso:encrypt:token:";

    public static String REDIS_AMP_INTERFACE_RANK = "amp:interface:rank";

    public static String REDIS_AMP_ACCESS = "amp:access:";

    public static String REDIS_OPERATION_LOG = "amp:operationLog";

    public static String REDIS_LOGIN_LOG = "amp:loginLog";

    public static long REDIS_LOCK_TIME_ONE_HOUR = 60*60*1L;

    public static long REDIS_LOCK_TIME_TEN_MINUTES = 10*60*1L;

    public static long REDIS_EXPIRE_TIME_ONE_HOUR= 60*60*1L;

    public static long REDIS_EXPIRE_TIME_TWO_HOUR= 60*60*2L;

}
