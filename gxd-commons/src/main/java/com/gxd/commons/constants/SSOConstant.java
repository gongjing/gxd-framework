package com.gxd.commons.constants;

/**
 * @Author:gxd
 * @Description:
 * @Date: 11:50 2018/9/4
 * @Modified By:
 */
public class SSOConstant {

    public static String GXD_SSO_SESSION_ID = "gxd_sso_sessionid";

    public static String SSO_USER = "gxd_sso_user";

    public static String STAFF_CODE = "_sc_";

    public static String STAFF_ID = "_si_";

    public static String LOGIN_NAME = "_ln_";

    public static String SSO_TOKEN = "_sso_token_";

    public static String IS_LOGIN = "_is_login_";

    /**
     * 无需登录验证
     */
    public static String UN_LOGIN_NO_AUTH = "0";

    /**
     * 登录验证
     */
    public static String LOGIN_AUTH = "1";

    /**
     * 忽略验证
     */
    public static String IGNORE_VALIDATION = "1";

    /**
     * 需要回调子系统
     */
    public static String IS_NEED_CALL = "1";

    /**
     * 不需要回调子系统
     */
    public static String NO_NEED_CALL = "0";


}
