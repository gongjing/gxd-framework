

package com.gxd.commons.constants;

public enum ExceptionConstant {

    SUCCESS_STATUS(00000, ""),

    ERROR(11111, "业务异常！"),

    /**
     * 发送邮件
     */
    NO_SEND_USER(0001, "发件人邮箱缺失！"),

    NO_SEND_PWD(0002, "发件人邮箱密码缺失！"),

    NO_TO_USER(0003, "收件人缺失！"),

    ERROR_FILE_PATH(0004, "附件地址错误！"),

    ERROR_SEND_PWDH(0005, "发件人用户名密码错误!"),

    /**
     * 阿里支付
     */

    NO_PARTNER(1001, "合作身份者ID缺失!"),

    NO_KEY(1002, "商户的私钥缺失!"),

    NO_TOTAL_FEE(1003, "订单总金额缺失!"),

    NO_OUT_TRADE_NO(1004, "商户网站唯一订单号缺失!"),

    NO_SUBJECT(1005, "商品的标题/交易标题/订单标题/订单关键字等缺失!"),

    ERROR_MD5(1006, "MD5签名过程中出现错误,指定的编码集不对!"),

    /**
     * FastDFS上传
     */
    NO_FILE_SIZE(2001, "上传文件大小缺失！"),

    NO_FILE_TYPE(2002, "允许上传文件类型缺失！"),

    NO_FILE_ITEMS(2003, "上传文件流缺失！"),

    NO_FILE_TYPE_ERROR(2004, "不允许上传此类型文件！"),

    NO_FILE_SIZE_ERROR(2005, "文件大小超过限制！"),

    NO_TRACKER_SERVER(2006, "上传服务器地址缺失！"),

    NO_HTTP_SECRET_KEY(2007, "上传密钥缺失！"),

    NO_HTTP_REQUEST(2008, "上传request缺失！"),

    NO_HTTP_RESPONSE(2009, "上传response缺失！"),

    NO_UPLOAD_FILE(2010, "上传文件缺失！"),

    /**
     * 隐私通话
     */
    NO_CHOOSENUMBER_CALL_REQUEST(3001, "ChooseNumberCallRequest对象缺失"),

    NO_ACCOUNT_SID(3002, "云之讯接入sid缺失!"),

    NO_APP_ID(3003, "云之讯应用id缺失"),

    NO_AUTH_TOKEN(3004, "云之讯接入token缺失"),

    NO_CITY_ID(3005, "云之讯提供中间手机号的归属地缺失"),

    NO_CALLEE(3006, "被绑定的手机号缺失"),

    NO_DS_TVIRTUAL_NUM(3007, "云之讯提供的中间手机号缺失"),

    NO_HANGUP_URL(3008, "话单推送地址缺失"),

    NO_MAX_AGE(3009, "解绑时间缺失"),

    NO_REQUEST_ID(3010, "返回id缺失"),

    ERROR_UCPAAS_CODE(3011, "云之讯错误码!"),

    NO_UNBIND_NUMBER_CALL_REQUEST(3012, "UnbindNumberCallRequest对象缺失"),

    NO_BIND_ID(3013, "绑定id缺失"),

    /**
     * 阿里云上传
     */
    NO_ALIYUN_URL(4001, "阿里云地址缺失"),

    NO_ENDPOINT(4002, "endpoint缺失！"),

    NO_ACCESSKEY_ID(4003, "accessKeyId缺失！"),

    NO_ACCESSKEY_SECRET(4004, "accessKeySecret缺失！"),

    NO_BUCKET_NAME(4005, "bucketName缺失！"),

    NO_ROLE_ARN(4006, "roleArn缺失！"),

    NO_ROLESESSION_NAME(4007, "roleSessionName缺失！"),

    NO_DURATION_SECONDS(4008, "durationSeconds缺失！"),

    /**
     * 阿里大于短信
     */
    NO_ALIDAYU_APP_ID(5001, "appId缺失！"),

    NO_ALIDAYU_APP_PWD(5002, "appPwd缺失！"),

    NO_ALIDAYU_SIGN_NAME(5003, "签名缺失！"),

    NO_ALIDAYU_CODE(5004, "短信内容缺失！"),

    NO_ALIDAYU_TRADING_PRO(5005, "业务名称缺失！"),

    NO_ALIDAYU_PHONE(5006, "发送手机号缺失！"),

    NO_ALIDAYU_TEMPLATEREG(5007, "模版编号缺失！"),

    NO_ALIDAYU_TEMPLATPARA(5008, "模版参数变量缺失！"),

    /**
     * ftp上传
     */

    NO_URL(6001, "地址缺失!"),

    ERROR_URL(6002, "连接出错!"),

    /**
     * 微信模块
     */
    NO_WEIXIN_OAUTH_URL_REQUEST(8001, "WeiXinOauthUrlRequest对象缺失"),

    NO_WEIXIN_URL(8002, "微信授权回调地址缺失"),

    NO_WEIXIN_APP_ID(8003, "微信应用appid缺失"),

    NO_WEIXIN_RESPONSE(8004, "HttpServletResponse缺失"),

    NO_WEIXIN_OAUTH2_TOKEN_REQUEST(8005, "WeixinOauth2TokenRequest对象缺失"),

    NO_WEIXIN_APP_SECRET(8006, "微信应用密钥缺失"),

    NO_WEIXIN_CODE(8007, "微信回调code缺失"),

    NO_WEIXIN_USER_INFO_REQUEST(8008, "WeixinUserInfoRequest对象缺失"),

    NO_WEIXIN_ACCESS_TOKEN(8009, "微信授权凭证token缺失"),

    NO_WEIXIN_OPEN_ID(8010, "微信用户openid缺失"),

    NO_WEIXIN_FIEDORDER_REQUEST(8011, "WeixinFiedorderRequest对象缺失"),

    NO_WEIXIN_MCH_ID(8012, "微信商户号缺失"),

    NO_WEIXIN_BODY(8013, "商品信息描述缺失"),

    NO_WEIXIN_ATTACH(8014, "自定义参数缺失"),

    NO_WEIXIN_OUT_TRADE_NO(8015, "商户订单号缺失"),

    NO_WEIXIN_TOTAL_FEE(8016, "订单总计费用缺失"),

    NO_WEIXIN_SPBILL_CREATE_IP(8017, "客户端地址缺失"),

    NO_WEIXIN_NOTIFY_URL(8018, "支付通知回调地址缺失"),

    NO_WEIXIN_TRADE_TYPE(8019, "交易类型缺失"),

    NO_WEIXIN_TRADE_NO(8020, "订单号缺失"),

    NO_WEIXIN_WEIIN_PARTNERKEY(8021, "微信API秘钥缺失"),

    ERRO_WEIXIN_TRADE_TYPE(8022, "交易类型错误"),

    ERRO_WEIXIN_PREPAY_ID(8023, "微信预支付失败"),

    /**
     * 文件操作
     */
    NO_SUPPORT_TYPE(9001, "不支持的文件类型！"),

    NO_TITLE(9002, "表的标题缺失！"),

    NO_OUT(9003, "输出流缺失！"),

    NO_FILE(9004, "文件缺失！"),

    ERROR_READ(9005, "读取文件内容出错！"),

    ERROR_STREAM_CLOSE(9006, "关闭流出错！"),

    ERROR_DOCUMENT_CLOSE(9007, "关闭文档出错！"),

    /**
     * 螺丝猫短信
     */
    NO_LSM_API_KEY(10001, "apiKey缺失！"),

    NO_LSM_URL(10002, "sendMsgUrl缺失！"),

    NO_LSM_PHONE(10003, "phone缺失！"),

    NO_LSM_CODE(10004, "code缺失！");

    private String msg;

    private Integer value;

    private ExceptionConstant(Integer value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
