/*
* Project:kepler-tool
* Author:陈泉
* Company:中润普达
* Created Date:	2017年8月18日 下午2:19:08
* Copyright(c) @ 2017  
* @version V3.0
*/

package com.gxd.commons.constants;

/**
 * 发送邮件常量类
 * 实例:SendMailConstant
 * 描述:TODO
 * 
 * 注意事项:
 *
 * 
 * @date 2017年8月21日 下午5:10:07
 * @author gxd
 */
public class SendMailConstant {

    // 设置服务器
    public static final String KEY_SMTP = "mail.smtp.host";
    public static final String VALUE_SMTP = "smtp.exmail.qq.com";
    // 服务器验证
    public static final String KEY_PROPS = "mail.smtp.auth";
    public static final String VALUE_PROPS = "true";
    // 发件人用户名、密码
    public static final String SEND_USER = "dataqu@bigdatadh.com";
    public static final String SEND_UNAME = "dataqu@bigdatadh.com";
    public static final String SEND_PWD = "Abc12345";

}
