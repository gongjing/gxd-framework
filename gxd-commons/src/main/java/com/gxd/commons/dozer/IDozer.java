package com.gxd.commons.dozer;

import java.util.List;
import java.util.Set;

/**
 * @Author:gxd
 * @Description:
 * @Date: 15:36 2018/10/30
 * @Modified By:
 */
public interface IDozer {
    /**
     * @Description: 单个对象的深度复制及类型转换，vo/domain , po
     * @param s 数据对象
     * @param clz 复制目标类型
     * @return
     * @author gxd
     * @Time 2018年10月30日 下午3:53:24
     */
    <T, S> T convert(S s, Class<T> clz);

    /**
     * @Description: list深度复制
     * @param s 数据对象
     * @param clz 复制目标类型
     * @return
     * @author gxd
     * @Time 2018年10月30日 下午3:54:08
     */
    <T, S> List<T> convert(List<S> s, Class<T> clz);

    /**
     * @Description: set深度复制
     * @param s 数据对象
     * @param clz 复制目标类型
     * @return
     * @author gxd
     * @Time 2018年10月30日 下午3:54:39
     */
    <T, S> Set<T> convert(Set<S> s, Class<T> clz);

    /**
     * @Description: 数组深度复制
     * @param s 数据对象
     * @param clz 复制目标类型
     * @return
     * @author gxd
     * @Time 2018年10月30日 下午3:54:57
     */
    <T, S> T[] convert(S[] s, Class<T> clz);
}
