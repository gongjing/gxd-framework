package com.gxd.commons.annotation;

import java.lang.annotation.*;

/**
 * @Author:gxd
 * @Description:操作日志注解
 * @Date: 10:07 2018/3/8
 * @Modified By:
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OperateLogAnnotation {
    /** 要执行的操作类型比如：add操作 **/
     String operateType() default "";

    /** 操作的表名 **/
     String operateObject() default "";

     /**表的主键**/
     String key() default "";

     /**操作描述，例如：注册账号**/
     String operateDec() default  "";

      /**api name，例如：** 接口名称**/
     String apiName() default  "";

    /**
     * 模块
     * @return
     */
    String module() default  "";

}
