package com.gxd.commons.enums;

/**
 * @Author:gxd
 * @Description:错误代码枚举类
 * @Date: 16:59 2018/7/9
 * @Modified By:
 */
public enum ResultCodeEnum {
    SUCCESS(0, "success"),

    FAIL(-1,"fail"),

    PARAM_EMPTY(1001, "必选参数为空"),

    PARAM_ERROR(1002, "参数格式错误"),

    PARAM_BIND_ERROR(1003, "参数验证失败"),

    FALL_BACK(9998, "服务熔断"),

    UNKNOWN_ERROR(9999, "系统繁忙，请稍后再试....");

    private int  status;

    private String desc;

    ResultCodeEnum(int status, String desc) {
        this.status = status;
        this.desc = desc;
    }

    public int getStatus() {
        return this.status;
    }


    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "ResultCodeEnum{" +
                "status='" + status + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
