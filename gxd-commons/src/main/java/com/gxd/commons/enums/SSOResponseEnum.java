package com.gxd.commons.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author:gxd
 * @Description:sso 单点登录的信息提示汇集
 * @Date: 17:48 2018/1/3
 * @Modified By:
 */
public enum SSOResponseEnum {
    LOGIN_NAME_NOT_NULL(1001,"账号不能为空."),
    PASSWORD_NOT_NULL(1002,"密码不能为空."),
    LOGIN_TOKEN_NOT_NULL(1003,"会话信息为空或已过期."),
    TOKEN_NOT_UNAUTHORIZED(1004, "用户token验证不通过."),
    SESSION_EXPIRE(1005, "会话已经过期，请重新登陆."),
    TOKEN_IN_VALID(1006, "令牌无效，身份为冒用."),
    TOKEN_IS_WRONG(1007, "令牌不正确，无权限访问系统."),
    ACCOUNT_LOGIN_EXCEPTION(1008, "您的账号已在其他地方登录，若不是本人操作，请修改密码！"),
    NETWORK_ENVIRONMENT_CHANGE(1009, "您的网络环境已经改变，为了保障安全，请您重新登陆！"),
    LOGIN_NAME_NOT_EXIST(1010, "账号不存在."),
    EMPLOYEE_TURNOVER(1011, "员工已离职，禁止使用."),
    ACCOUNT_FREEZING(1012, "账号已被冻结，禁止使用."),
    MUCH_PASSWORD_ERROR(1013,"由于密码输入错误次数大于%s次，请%s小时后再试！"),
    WRONG_PASSWORD(1014,"密码错误."),
    LOGIN_SIGN(1015,"登录"),
    LOGIN_OUT_SIGN(1016,"退出"),
    TOKEN_EXPIRE(1017, "用户token已过期，请重新登陆."),
    LOGIN_OUT_SUCCESS(1018, "退出成功."),
    PASSWORD_REMAINDER_TIMES(1019, "密码错误，您还有%s次机会."),
    LOGIN_TOKEN_IS_NULL(1020, "未检测到登录信息，请先登录再试."),
    PASSWORD_OUT_DATE(1021, "密码已过期，请联系管理员修改密码"),
    PASSWORD_NEED_CHANGE(1022, "密码还有%s天过期，请尽快修改密码"),
    RESPONSE_CODE_UNLOGIN_ERROR(9999, "系统异常");
    // 成员变量
    private int code; //状态码
    private String message; //返回消息

    // 构造方法
    private SSOResponseEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public static Map<String, Object> buildReturnMap(SSOResponseEnum responseCode, Object data) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", responseCode.getCode());
        map.put("message", responseCode.getMessage());
        map.put("data", data);
        return map;
    }
    public static SSOResponseEnum getMessageByCode(int code){
        for(SSOResponseEnum ssoResponseEnum : SSOResponseEnum.values()){
            if(code == ssoResponseEnum.getCode()){
                return ssoResponseEnum;
            }
        }
        return null;
    }
}
