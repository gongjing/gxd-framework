package com.gxd.commons.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author:gxd
 * @Description:web页面的信息提示汇集
 * @Date: 17:48 2018/1/3
 * @Modified By:
 */
public enum ResponseEnum {
    TOKEN_IS_NULL(2001,"会话已经过期，请刷新重新登录!"),
    TOKEN_DECRYPT_FAIL(2002,"解密token失败，请刷新重新登录!"),
    SESSION_EXPIRE(2003,"会话已经过期，请刷新重新登录！"),
    TOKEN_IN_VALID(2004, "令牌无效，请刷新重新登录！"),
    TOKEN_IS_WRONG(2005, "令牌不正确，无权限访问系统！"),
    ACCOUNT_LOGIN_EXCEPTION(2006, "您的账号已在其他地方登录，若不是本人操作，请刷新重新登录并修改密码！"),
    NETWORK_ENVIRONMENT_CHANGE(2007, "您的网络环境已经改变，为了保障安全，请刷新重新登录！"),
    AUTH_TOKEN_IS_NULL(2008, "未检测到登录信息，请先登录再试！"),
    AUTH_TOKEN_IN_VALID(2009, "解密token失败，请先登录再试！"),
    AUTH_SESSION_EXPIRE(2010, "未检测到登录信息，session已经过期，请先登录再试！"),
    NO_ANY_AUTH(2011, "您当前没有任何菜单访问权限，请联系管理员！"),
    NO_CURRENT_AUTH(2012, "您没有当前菜单操作权限，请联系管理员！"),
    NO_OPERATE_AUTH(2013, "您没有操作权限，请联系管理员！"),
    TOKEN_EXPIRE(2014, "用户token已过期，请重新登陆.");

    /**
     * 成员变量
     */
    /**
     * 状态码
     */
    private int code;
    /**
     * 返回消息
     */
    private String message;

    /**
     * 构造方法
      */
    private ResponseEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public static Map<String, Object> buildReturnMap(ResponseEnum responseCode, Object data) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("code", responseCode.getCode());
        map.put("message", responseCode.getMessage());
        map.put("data", data);
        return map;
    }
    public static ResponseEnum getMessageByCode(int code){
        for(ResponseEnum ssoResponseEnum : ResponseEnum.values()){
            if(code == ssoResponseEnum.getCode()){
                return ssoResponseEnum;
            }
        }
        return null;
    }
}
