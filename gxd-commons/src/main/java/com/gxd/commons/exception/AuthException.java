package com.gxd.commons.exception;

import com.gxd.commons.model.Result;

public class AuthException extends RuntimeException {

    private static final long serialVersionUID = 1144969267587138347L;

    public AuthException(String code, String message, Exception cause) {
        super(code + ":" + message, cause);
    }

    public AuthException(String code, String message) {
        super(code + ":" + message);
    }

    public AuthException() {
        super();
    }

    public AuthException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthException(String message) {
        super(message);
    }

    public AuthException(Throwable cause) {
        super(cause);
    }

    public static void throwMessage(String errCode) {
        throw new AuthException(errCode);
    }
    /**
     * <pre>
     * 抛出业务逻辑异常信息
     * </pre>
     */
    public static void throwMessage(String errCode, String... params) {
        throw new AuthException(errCode, errCode);
    }

    /**
     * <pre>
     * 抛出业务逻辑异常信息
     * </pre>
     */
    public static void throwMessageWithCode(String errCode, String message) {
        throw new AuthException(errCode, message);
    }

    /**
     * <pre>
     * 抛出业务逻辑异常信息
     * </pre>
     */
    public static void throwResult(Result result) {
        String errCode = String.valueOf(result.getState());
        String message = result.getMessage();
        throw new AuthException(errCode, message);
    }
}
