package com.gxd.commons.exception;

public interface ErrorCode {

    int getCode();

    String getMessage();
}
