

package com.gxd.commons.exception;

/**
 * 自定义服务异常（包括错误代码和信息）
 * 实例:ServiceException
 * 描述:TODO
 * 
 * 注意事项:
 *
 * 
 * @date 2017年8月18日 下午2:09:20
 * @author gxd
 */
public class ToolException extends RuntimeException {

    /**
      * @Fields serialVersionUID : TODO（用一句话描述这个变量表示什么）
      */
    private static final long serialVersionUID = 1L;

    protected int errorCode = 10001; // 系统异常错误码默认为10001

    protected String errorMessage = null; // 错误消息默认为null

    public ToolException(int errorCode, String msg) {
        super(msg);
        this.errorMessage = msg;
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
