package com.gxd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import zipkin.server.EnableZipkinServer;

@SpringBootApplication
@EnableZipkinServer
@EnableDiscoveryClient //向Eureka注册
public class GxdSleuthZipkinApplication {

	public static void main(String[] args) {
		SpringApplication.run(GxdSleuthZipkinApplication.class, args);
	}
}
