package com.gxd.datasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @Author:gxd
 * @Description: 动态数据源
 * @Date: 13:13 2017/12/27
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    private static final Logger log = LoggerFactory.getLogger(DynamicDataSource.class);

    /**
     * 设置当前的数据源，在路由类中使用getDataSourceType进行获取，
     * 交给AbstractRoutingDataSource进行注入使用。
     * @return
     */
    @Override
    protected Object determineCurrentLookupKey() {
        log.debug("数据源为{}", DynamicDataSourceContextHolder.getDataSourceType() == null?"默认数据源":DynamicDataSourceContextHolder.getDataSourceType());
        return DynamicDataSourceContextHolder.getDataSourceType();
    }

}
