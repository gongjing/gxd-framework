package com.gxd;

import com.gxd.redis.utils.RedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GxdComponentsRedisApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private RedisUtils redisUtils;

	@Test
	public void testRedisTPS() throws InterruptedException {
		int poolSize = 15000;
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch end = new CountDownLatch(poolSize);
		ExecutorService exce = Executors.newFixedThreadPool(poolSize);
		for (int i = 0; i < poolSize; i++) {
			Runnable run = new Runnable() {
				@Override
				public void run() {
					try {
						start.await();
						testCounter();
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						end.countDown();
					}
				}
			};
			exce.submit(run);
		}
		start.countDown();
		end.await();
		exce.shutdown();
	}

	private void testTPS() {
		if (redisUtils.controlTPS("test", "count", 10L)) {
			System.out.println("tps超限");
		} else {
			System.out.println("通过");
		}
	}


	public void testTPS1() {
		if (redisUtils.redisRateLimiter("limitKey", 60 * 24, 1000000) != 1) {
			System.out.println("tps超限");
			System.out.println("通过");
		}
	}

	public void testTPS2() {
		List<String> keys = new ArrayList();
		keys.add("{limitKey}limitKey");
		keys.add("{limitKey}countKey");
		List<String> args = new ArrayList();
		args.add("100");
		args.add("10000000");
		//redisUtils.redisQuantityLimiter("{limitKey}countKey",6000000);
		List<String> list = redisUtils.redisRateLimiterAndCount(keys, args);
		String s = list.get(0);
		String s2 = list.get(1);
		if (!s.equals("1")) {
			System.out.println("tps超限");
		}
		System.out.println("日调用量：" + s2);
	}

	public void testCounter() {
		if (redisUtils.redisQuantityLimiter("countKey", 60000, 8001) == 0) {
			System.out.println("日调用量超限");
		} else {
			System.out.println("通过");
		}
	}

	@Test
	public void testCounter1() {

	}
	//@Rule
	//public ContiPerfRule i = new ContiPerfRule();
	//@Test
	//@PerfTest(invocations = 1000, threads = 100)
	//@Required(max = 12000, average = 250, totalTime = 60000)
	//public void test1() throws Exception {
	//	if (redisUtils.redisQuantityLimiter("countKey", 600, 4000) == 0) {
	//		System.out.println("日调用量超限");
	//	} else {
	//		System.out.println("通过");
	//	}
	//}
}
