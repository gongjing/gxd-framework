package com.gxd.rocketmq.config;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @Author:gxd
 * @Description:
 * @Date: 14:40 2018/3/7
 * @Modified By:
 */
@Service
public class AsyncTask {
    public static final Logger log = LoggerFactory.getLogger(AsyncTask.class);

    @Async
    public void doAsyncTask(DefaultMQPushConsumer consumer){
        try {
            consumer.start();
            log.debug("消费者开启成功..........");
        } catch (MQClientException e) {
            log.error("异常信息：" + e.getErrorMessage());
        }
    }
}
