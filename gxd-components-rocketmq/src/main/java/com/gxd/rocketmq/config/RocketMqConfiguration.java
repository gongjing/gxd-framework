package com.gxd.rocketmq.config;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import com.gxd.rocketmq.event.RocketMqEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Set;

/**
 * @Author:gj
 * @Date: 8:36 2018/1/11
 * @Description:
 */
@Configuration
@EnableConfigurationProperties(RocketMqProperties.class)
public class RocketMqConfiguration {
    public static final Logger log = LoggerFactory.getLogger(RocketMqConfiguration.class);

	@Autowired
	private RocketMqProperties rmqProperties;

	@Autowired
	private ApplicationEventPublisher publisher;
	//@Autowired
	//private ApplicationContext applicationContext;
	@Autowired
	private AsyncTask asyncTask;

	/**
	 * 发送普通消息
	 */
	@Bean(name = "defaultMQProducer")
	public DefaultMQProducer defaultMQProducer() throws MQClientException {
		DefaultMQProducer producer = new DefaultMQProducer(rmqProperties.getDefaultProducer());
		producer.setNamesrvAddr(rmqProperties.getNamesrvAddr());
		producer.setInstanceName(rmqProperties.getInstanceName());
		producer.setMaxMessageSize(rmqProperties.maxMessageSize);
		producer.setSendMsgTimeout(rmqProperties.sendMsgTimeout);
		producer.setVipChannelEnabled(false);
		producer.start();
		//System.out.println("DefaultMQProducer is Started.");
		log.info("生产者DefaultMQProducer启动成功..........");
		return producer;
	}

	/**
	 * 发送事务消息
	 */
	@Bean(name = "transactionMQProducer")
	public TransactionMQProducer transactionMQProducer() throws MQClientException {
		TransactionMQProducer producer = new TransactionMQProducer(rmqProperties.getTransactionProducer());
		producer.setNamesrvAddr(rmqProperties.getNamesrvAddr());
		producer.setInstanceName(rmqProperties.getInstanceName());
		producer.setMaxMessageSize(rmqProperties.maxMessageSize);
		producer.setSendMsgTimeout(rmqProperties.sendMsgTimeout);
		producer.setTransactionCheckListener((MessageExt msg) ->{
			System.out.println("事务回查机制！");
			return  LocalTransactionState.COMMIT_MESSAGE;
		});
		// 事务回查最小并发数
		producer.setCheckThreadPoolMinSize(2);
		// 事务回查最大并发数
		producer.setCheckThreadPoolMaxSize(5);
		// 队列数
		producer.setCheckRequestHoldMax(2000);
		producer.start();
		log.info("生产者TransactionMQProducer启动成功..........");
		return producer;
	}

	/**
	 * 消费者
	 */
	@Bean("pushConsumer")
	public DefaultMQPushConsumer pushConsumer() throws MQClientException {
		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(rmqProperties.getDefaultConsumer());
		Set<String> setTopic = rmqProperties.getDefaultTopic();
		for (String topic : setTopic) {
			consumer.subscribe(topic, "*");
		}
		consumer.setNamesrvAddr(rmqProperties.getNamesrvAddr());
		consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
		consumer.setConsumeThreadMin(rmqProperties.consumeThreadMin);
		consumer.setConsumeThreadMax(rmqProperties.consumeThreadMax);
		//一次消费一条
		consumer.setConsumeMessageBatchMaxSize(rmqProperties.consumerMessageBatchMaxSize);
		consumer.registerMessageListener((List<MessageExt> msgs, ConsumeConcurrentlyContext context) -> {
				MessageExt msg = msgs.get(0);
				try {
					publisher.publishEvent(new RocketMqEvent(msg, consumer));
				} catch (Exception e) {
					if (msg.getReconsumeTimes() == 3) {
						return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;// 成功
					} else {
						System.out.println("定时重试！");
						return ConsumeConcurrentlyStatus.RECONSUME_LATER;// 重试
					}
				}
				return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
		});
		asyncTask.doAsyncTask(consumer);
		return consumer;
	}
	/**
	 * 顺序消费者
	 */
	@Bean("pushOrderConsumer")
	public DefaultMQPushConsumer pushOrderConsumer() throws MQClientException {
		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(rmqProperties.getOrderConsumer());
		Set<String> setTopic = rmqProperties.getOrderTopic();
		for (String topic : setTopic) {
			consumer.subscribe(topic, "*");
		}
		consumer.setNamesrvAddr(rmqProperties.getNamesrvAddr());
		consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
		consumer.setConsumeMessageBatchMaxSize(1);
		consumer.registerMessageListener((List<MessageExt> msgs, ConsumeOrderlyContext context) -> {
				MessageExt msg = msgs.get(0);
				try {
					publisher.publishEvent(new RocketMqEvent(msg, consumer));
				} catch (Exception e) {
					if (msg.getReconsumeTimes() <= 1) {
						return ConsumeOrderlyStatus.SUCCESS;
					} else {
						System.out.println("定时重试！");
					}
				}
				return ConsumeOrderlyStatus.SUCCESS;
		});
		asyncTask.doAsyncTask(consumer);
		return consumer;
	}

}
