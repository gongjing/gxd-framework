package com.gxd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


//@EnableHystrixDashboard
//@EnableHystrix
//@EnableCircuitBreaker
//@EnableDiscoveryClient //向Eureka注册
//@ServletComponentScan
@SpringBootApplication
public class GxdComponentsFastdfsApplication extends WebMvcConfigurerAdapter {
	private static final Logger logger = LoggerFactory.getLogger(GxdComponentsFastdfsApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(GxdComponentsFastdfsApplication.class, args);
		logger.debug("上传文件微服务启动成功！");
	}
	/**
	 *  favorPathExtension表示支持后缀匹配，
	 *  属性ignoreAcceptHeader默认为fasle，表示accept-header匹配，
	 *  defaultContentType开启默认匹配。
	 * @param configurer
	 */
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(false);
	}
}

