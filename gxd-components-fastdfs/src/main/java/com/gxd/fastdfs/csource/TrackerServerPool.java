package com.gxd.fastdfs.csource;

import com.google.common.collect.Maps;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.csource.common.MyException;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.TrackerServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * TrackerServer 对象池
 * <p>
 *
 * @author jiangzhou.bo@hand-china.com
 * @version 1.0
 * @date 2017-10-14 15:23
 */
public class TrackerServerPool {
    /**
     * org.slf4j.Logger
     */
    private static Logger logger = LoggerFactory.getLogger(TrackerServerPool.class);

    /**
     * 最大连接数 default 8.
     */
    private static int maxStorageConnection = 200;

    private static String connectTimeoutInSeconds = "60";

    private static String networkTimeoutInSeconds = "60";

    private static String charset = "UTF-8";

    private static String httpTrackerHttpPort = "8081";

    private static String httpAntiStealToken = "no";

    private static String httpSecretKey = "FastDFS1234567890";

    //private static String trackerServers = "192.168.50.36:22122,192.168.50.37:22122";

    private static String trackerServers = "192.168.13.5:22122,192.168.13.6:22122";


    /**
     * TrackerServer 对象池.
     * GenericObjectPool 没有无参构造
     */
    private static GenericObjectPool<TrackerServer> trackerServerPool;

    private TrackerServerPool(){

    };

    private static synchronized GenericObjectPool<TrackerServer> getObjectPool(){
        if(trackerServerPool == null){
            try {
                // 加载配置文件
                Map map = Maps.newHashMap();
                map.put("fastdfs.connect_timeout_in_seconds", connectTimeoutInSeconds);
                map.put("fastdfs.network_timeout_in_seconds",networkTimeoutInSeconds);
                map.put("fastdfs.charset",charset);
                map.put("fastdfs.http_tracker_http_port",httpTrackerHttpPort);
                map.put("fastdfs.http_anti_steal_token",httpAntiStealToken);
                map.put("fastdfs.http_secret_key",httpSecretKey);
                map.put("fastdfs.tracker_servers",trackerServers);
                ClientGlobal.initFastDFS(map);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (MyException e) {
                e.printStackTrace();
            }
            if(logger.isDebugEnabled()){
                logger.debug("ClientGlobal configInfo: {}", ClientGlobal.configInfo());
            }
            // Pool配置
            GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
            poolConfig.setMinIdle(2);
            if(maxStorageConnection > 0){
                poolConfig.setMaxTotal(maxStorageConnection);
            }
            trackerServerPool = new GenericObjectPool<>(new TrackerServerFactory(), poolConfig);
        }
        return trackerServerPool;
    }

    /**
     * 获取 TrackerServer
     * @return TrackerServer
     * @throws FastDFSException
     */
    public static TrackerServer borrowObject() throws FastDFSException {
        TrackerServer trackerServer = null;
        try {
            trackerServer = getObjectPool().borrowObject();
        } catch (Exception e) {
            e.printStackTrace();
            if(e instanceof FastDFSException){
                throw (FastDFSException) e;
            }
        }
        return trackerServer;
    }

    /**
     * 回收 TrackerServer
     * @param trackerServer 需要回收的 TrackerServer
     */
    public static void returnObject(TrackerServer trackerServer){

        getObjectPool().returnObject(trackerServer);
    }


}
