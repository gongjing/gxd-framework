package com.gxd.fastdfs.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author:gxd
 * @Description:
 * @Date: 16:17 2018/3/27
 * @Modified By:
 */
@ConfigurationProperties(prefix = "sftp")
@ConditionalOnProperty(value = "sftp.enabled", havingValue = "true")
@Component("sftpConfigProperties")
public class SFTPConfigProperties {

    private String host = "localhost";

    private int port = 22;

    private String username;

    private String password;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
