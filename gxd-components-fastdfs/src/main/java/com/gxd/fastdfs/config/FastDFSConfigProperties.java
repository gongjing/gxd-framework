package com.gxd.fastdfs.config;

import com.google.common.collect.Maps;
import org.csource.common.MyException;
import org.csource.fastdfs.ClientGlobal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Map;

/**
 * @Author:gxd
 * @Description:
 * @Date: 16:17 2018/3/27
 * @Modified By:
 */
@ConfigurationProperties(prefix = "fastdfs")
@ConditionalOnProperty(value = "fastdfs.enabled", havingValue = "true")
@Component
public class FastDFSConfigProperties {
    private static Logger log = LoggerFactory.getLogger(FastDFSConfigProperties.class);

    private FastDFSConfigProperties fastDFSConfigProperties;

    private String connectTimeoutInSeconds;
    private String networkTimeoutInSeconds;
    private String charset;
    private String httpTrackerHttpPort;
    private String httpAntiStealToken;

    private String httpSecretKey;
    private String trackerServers;
    private String webServerUrl;

    @PostConstruct
    public void init() {
        try {
            fastDFSConfigProperties = this;
            Map map = Maps.newHashMap();
            map.put("fastdfs.connect_timeout_in_seconds", fastDFSConfigProperties.getConnectTimeoutInSeconds());
            map.put("fastdfs.network_timeout_in_seconds",fastDFSConfigProperties.getNetworkTimeoutInSeconds());
            map.put("fastdfs.charset",fastDFSConfigProperties.getCharset());
            map.put("fastdfs.http_tracker_http_port",fastDFSConfigProperties.getHttpTrackerHttpPort());
            map.put("fastdfs.http_anti_steal_token = no",fastDFSConfigProperties.getHttpAntiStealToken());
            map.put("fastdfs.http_secret_key",fastDFSConfigProperties.getHttpSecretKey());
            map.put("fastdfs.tracker_servers",fastDFSConfigProperties.getTrackerServers());
            ClientGlobal.initFastDFS(map);
            log.debug("初始化fastdfs配置成功");
        } catch (IOException e) {
            log.debug("初始化fastdfs配置失败："+e.getMessage());
        } catch (MyException e) {
            log.debug("初始化fastdfs配置失败："+e.getMessage());
        }
    }

    public String getConnectTimeoutInSeconds() {
        return connectTimeoutInSeconds;
    }

    public void setConnectTimeoutInSeconds(String connectTimeoutInSeconds) {
        this.connectTimeoutInSeconds = connectTimeoutInSeconds;
    }

    public String getNetworkTimeoutInSeconds() {
        return networkTimeoutInSeconds;
    }

    public void setNetworkTimeoutInSeconds(String networkTimeoutInSeconds) {
        this.networkTimeoutInSeconds = networkTimeoutInSeconds;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getHttpTrackerHttpPort() {
        return httpTrackerHttpPort;
    }

    public void setHttpTrackerHttpPort(String httpTrackerHttpPort) {
        this.httpTrackerHttpPort = httpTrackerHttpPort;
    }

    public String getHttpAntiStealToken() {
        return httpAntiStealToken;
    }

    public void setHttpAntiStealToken(String httpAntiStealToken) {
        this.httpAntiStealToken = httpAntiStealToken;
    }

    public String getHttpSecretKey() {
        return httpSecretKey;
    }

    public void setHttpSecretKey(String httpSecretKey) {
        this.httpSecretKey = httpSecretKey;
    }

    public String getTrackerServers() {
        return trackerServers;
    }

    public void setTrackerServers(String trackerServers) {
        this.trackerServers = trackerServers;
    }

    public String getWebServerUrl() {
        return webServerUrl;
    }

    public void setWebServerUrl(String webServerUrl) {
        this.webServerUrl = webServerUrl;
    }
}
