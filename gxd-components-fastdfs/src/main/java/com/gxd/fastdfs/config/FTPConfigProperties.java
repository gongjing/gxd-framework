package com.gxd.fastdfs.config;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Author:gxd
 * @Description:
 * @Date: 11:05 2018/3/27
 * @Modified By:
 */
@ConfigurationProperties(prefix = "ftp")
public class FTPConfigProperties {
    private String host = "localhost";

    private int port = FTPClient.DEFAULT_PORT;

    private String username;

    private String password;

    /**
     * 缓冲区改为10M
     */
    private int bufferSize = 1024 * 1024 * 50;

    /**
     * 初始化连接数
     */
    private Integer initialSize = 0;

    private String encoding ="UFT-8";

    //FTP服务器上的相对路径
    private String remotePath="/picture";

    public String getRemotePath() {
        return remotePath;
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public Integer getInitialSize() {
        return initialSize;
    }

    public void setInitialSize(Integer initialSize) {
        this.initialSize = initialSize;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
