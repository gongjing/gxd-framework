package com.gxd;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.AppendFileStorageClient;
import com.google.common.collect.Lists;
import com.gxd.commons.utils.FastJsonUtils;
import com.gxd.fastdfs.client.FastDFSClient;
import com.gxd.fastdfs.csource.CFastDFSClient;
import com.gxd.fastdfs.csource.FastDFSException;
import com.gxd.fastdfs.csource.FastDfsClient;
import com.gxd.fastdfs.csource.FastDfsFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.net.InetSocketAddress;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GxdComponentsFastdfsApplicationTests {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FastDfsClient fastDfsClient;

	@Autowired
	private FastDFSClient fastDFSClient;
	//@Value("${fastdfs.trackerServers:192.168.13.5:22122,192.168.13.6:2212}")
	//private  String trackerServers;

	@Value("${fastdfs.trackerServers:192.168.50.36:22122,192.168.50.37:22122}")
	private String trackerServers = "192.168.50.36:22122,192.168.50.37:22122";

	@Test
	public void contextLoads() {
	}


	@Test
	public void latchTest() throws InterruptedException {
		int poolSize = 100;
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch end = new CountDownLatch(poolSize);
		ExecutorService exce = Executors.newFixedThreadPool(poolSize);
		for (int i = 0; i < poolSize; i++) {
			Runnable run = new Runnable() {
				@Override
				public void run() {
					try {
						start.await();
						testLoad();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						end.countDown();
					}
				}
			};
			exce.submit(run);
		}
		start.countDown();
		end.await();
		exce.shutdown();
	}

	public synchronized void testLoad() {
		String path = "";
		String[] fileAbsolutePath = {};
		String filePath = "C:\\Users\\Army\\Desktop\\servererror.png";
		File file = new File(filePath);
		try {
			byte[] fileBuff = null;
			for (int i = 0; i < 10000; i++) {
				InputStream inputStream = new FileInputStream(file);
				if (inputStream != null) {
					int len1 = inputStream.available();
					fileBuff = new byte[len1];
					inputStream.read(fileBuff);
				}
				inputStream.close();
				FastDfsFile fastDFSFile = new FastDfsFile("servererror.png", fileBuff, "png");
				try {
					fileAbsolutePath = fastDfsClient.upload(fastDFSFile);  //upload to fastdfs
				} catch (Exception e) {
					e.getStackTrace();
				}
				if (fileAbsolutePath == null) {
					logger.error("upload file failed,please upload again!");
				}
				path = fastDfsClient.getTrackerUrl() + fileAbsolutePath[0] + "/" + fileAbsolutePath[1];

				logger.debug("path:" + path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testLatch() throws InterruptedException {
		int poolSize = 2000;
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch end = new CountDownLatch(poolSize);
		ExecutorService exce = Executors.newFixedThreadPool(poolSize);
		for (int i = 0; i < poolSize; i++) {
			Runnable run = new Runnable() {
				@Override
				public void run() {
					try {
						start.await();
						testUpload();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						end.countDown();
					}
				}
			};
			exce.submit(run);
		}
		start.countDown();
		end.await();
		exce.shutdown();
	}

	@Test
	public void testLatch1() throws InterruptedException {
		final int count = 500; // 计数次数
		final CountDownLatch latch = new CountDownLatch(count);
		for (int i = 0; i < count; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						// do anything
						testUpload();
						System.out.println("线程"
								+ Thread.currentThread().getId());
					} catch (Throwable e) {
						// whatever
					} finally {
						// 很关键, 无论上面程序是否异常必须执行countDown,否则await无法释放
						latch.countDown();
					}
				}
			}).start();
		}
		try {
			// 线程countDown()都执行之后才会释放当前线程,程序才能继续往后执行
			latch.await();
		} catch (InterruptedException e) {
			// whatever
		}
		System.out.println("Finish");
	}

	public synchronized void testUpload() {
		InputStream inputStream = null;
		try {
			for (int i = 0; i < 10000; i++) {
				String path = "";
				String filePath = "C:\\Users\\Army\\Desktop\\1.png";
				File file = new File(filePath);
				inputStream = new FileInputStream(file);
				long size = inputStream.available();
				path = fastDFSClient.upload(inputStream, size, "1.png");
				inputStream.close();
				logger.debug("path:" + path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	@Autowired
	protected AppendFileStorageClient storageClient;


	@Test
	public void testLatch2() throws InterruptedException {
		final int count = 100; // 计数次数
		final CountDownLatch latch = new CountDownLatch(count);
		for (int i = 0; i < count; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						// do anything
						for (int i = 0; i < 100000; i++) {
							testUpload1();
						}
						System.out.println("线程"
								+ Thread.currentThread().getId());
					} catch (Throwable e) {
						// whatever
					} finally {
						// 很关键, 无论上面程序是否异常必须执行countDown,否则await无法释放
						latch.countDown();
					}
				}
			}).start();
		}
		try {
			// 线程countDown()都执行之后才会释放当前线程,程序才能继续往后执行
			latch.await();
		} catch (InterruptedException e) {
			// whatever
		}
		System.out.println("Finish");
	}



	@Test
	public  void testUpload1() {
		CloseableHttpClient httpClient = null;
		CloseableHttpResponse httpResponse = null;
		try {
			httpClient = HttpClientBuilder.create().build();
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(20000).setSocketTimeout(20000).build();
			//HttpPost httpPost = new HttpPost("http://192.168.50.35:8087/file/upload1");
			HttpPost httpPost = new HttpPost("http://127.0.0.1:8767/file/upload1?keyCode=af140809e47744a49bd24ff3c99ddffa");
			httpPost.setConfig(requestConfig);
			MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
			String filePath = "C:\\Users\\Army\\Desktop\\1.png";
			File file = new File(filePath);
			//multipartEntityBuilder.addBinaryBody("file", file,ContentType.create("image/png"),"abc.pdf");
			//当设置了setSocketTimeout参数后，以下代码上传PDF不能成功，将setSocketTimeout参数去掉后此可以上传成功。上传图片则没有个限制
			//multipartEntityBuilder.addBinaryBody("file",file,ContentType.create("application/octet-stream"),"abd.pdf");
			multipartEntityBuilder.addBinaryBody("file", file);
			List<NameValuePair> params = Lists.newArrayList();
			//params.add(new BasicNameValuePair("keyCode", "af140809e47744a49bd24ff3c99ddffa"));

			//multipartEntityBuilder.addPart("comment", new StringBody("This is comment", ContentType.TEXT_PLAIN));
			multipartEntityBuilder.addTextBody("comment", "this is comment");
			HttpEntity httpEntity = multipartEntityBuilder.build();
			httpPost.setEntity(httpEntity);
			//转换参数并设置编码格式
			//httpPost.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));

			httpResponse = httpClient.execute(httpPost);
			HttpEntity responseEntity = httpResponse.getEntity();
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(responseEntity.getContent()));
				StringBuffer buffer = new StringBuffer();
				String str = "";
				while (!StringUtils.isBlank(str = reader.readLine())) {
					buffer.append(str);
				}
				System.out.println(buffer.toString());
			}


		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(httpClient != null){
				try {
					httpClient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Test
	public void testUploadByGateway() {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			String url = "http://127.0.0.1:8767/file/upload1?keyCode=af140809e47744a49bd24ff3c99ddffa";

			//String url = "http://testlbsapi.cindata.net/file/upload1?keyCode=c1d0679650dc4a148a7b1e37518604ab";
			HttpPost httppost = new HttpPost(url);
			MultipartEntity reqEntity = new MultipartEntity();
			String filePath = "C:\\Users\\Army\\Desktop\\数据权限设计.png";

			FileBody filebdody = new FileBody(inputstreamtofile(
					new FileInputStream(new File(filePath)), "test.png"));
			reqEntity.addPart("file", filebdody);
			httppost.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			Map<String, String> newMap = new HashMap<String, String>();
			if (statusCode == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity());
				result = result.replace("\"", "");
				if (result.indexOf("上传成功") > -1 && null != result.split(":")[1]) {
					newMap.put("url", result.substring(8));
				} else {
					newMap.put("msg", result);
				}
				System.out.println(FastJsonUtils.toJSONString(newMap));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUploadByGatewayV2() {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			String url = "http://127.0.0.1:8767/file/v2/upload1?keyCode=af140809e47744a49bd24ff3c99ddffa";

			//String url = "http://testlbsapi.cindata.net/file/v2/upload1?keyCode=38902ecc095844f78dfd523f5a18e55b";
			HttpPost httppost = new HttpPost(url);
			MultipartEntity reqEntity = new MultipartEntity();
			String filePath = "C:\\Users\\Army\\Desktop\\数据权限设计.png";

			FileBody filebdody = new FileBody(inputstreamtofile(
					new FileInputStream(new File(filePath)), "test.png"));
			reqEntity.addPart("file", filebdody);
			httppost.setEntity(reqEntity);
			HttpResponse response = httpClient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity());
				System.out.println(FastJsonUtils.toJSONString(result));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUpload2ByGateway() {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			String url = "http://testlbsapi.cindata.net/file/upload2?keyCode=38902ecc095844f78dfd523f5a18e55b&path=http://filesvr.cindata.cn/group2/M00/46/6B/o4YBAFuY4xKADfvWAACh4GuL1nI373.png";
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpClient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity());
				System.out.println(FastJsonUtils.toJSONString(result));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testUpload2ByGatewayV2() {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			String url = "http://testlbsapi.cindata.net/file/v2/upload2?keyCode=c1d0679650dc4a148a7b1e37518604ab&path=http://filesvr.cindata.cn/group2/M00/46/6B/o4YBAFuY4xKADfvWAACh4GuL1nI373.png";
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpClient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity());
				System.out.println(FastJsonUtils.toJSONString(result));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Test
	public void testDownloadByGateway() {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			String url = "http://127.0.0.1:8767/file/v2/download?keyCode=af140809e47744a49bd24ff3c99ddffa&path=http://192.168.50.34:8088/dfs/group2/M00/00/00/pIYBAFt2cUqAWRepAAEHE-c1eNk898.png&fileName=test.png";
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpClient.execute(httppost);
			int statusCode = response.getStatusLine().getStatusCode();
			Map<String, String> newMap = new HashMap<String, String>();
			if (statusCode == HttpStatus.SC_OK) {
				String result = EntityUtils.toString(response.getEntity());
				result = result.replace("\"", "");
				if (result.indexOf("上传成功") > -1 && null != result.split(":")[1]) {
					newMap.put("url", result.substring(8));
				} else {
					newMap.put("msg", result);
				}
				System.out.println(FastJsonUtils.toJSONString(newMap));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}



	public File inputstreamtofile(InputStream ins, String filename)
			throws IOException {
		File file = new File(filename);
		OutputStream os = new FileOutputStream(file);
		int bytesRead = 0;
		byte[] buffer = new byte[8192];
		while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
			os.write(buffer, 0, bytesRead);
		}
		os.close();
		ins.close();
		return file;
	}

	@Test
	public void csourcePoolTest() throws InterruptedException {
		int poolSize = 100;
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch end = new CountDownLatch(poolSize);
		ExecutorService exce = Executors.newFixedThreadPool(poolSize);
		for (int i = 0; i < poolSize; i++) {
			Runnable run = new Runnable() {
				@Override
				public void run() {
					try {
						start.await();
						testCsourcePool();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						end.countDown();
					}
				}
			};
			exce.submit(run);
		}
		start.countDown();
		end.await();
		exce.shutdown();
	}

	private void testCsourcePool() {
		String path = "";
		String[] fileAbsolutePath = {};
		String filePath = "C:\\Users\\Army\\Desktop\\2.png";
		File file = new File(filePath);
		try {
			byte[] fileBuff = null;
			for (int i = 0; i < 10000; i++) {
				InputStream inputStream = new FileInputStream(file);
				if (inputStream != null) {
					int len1 = inputStream.available();
					fileBuff = new byte[len1];
					inputStream.read(fileBuff);
				}
				String ext = filePath.substring(filePath.lastIndexOf(".") + 1);
				FastDfsFile fastDFSFile = new FastDfsFile(filePath, fileBuff, ext);
				CFastDFSClient cFastDFSClient = new CFastDFSClient();
				fileAbsolutePath = cFastDFSClient.uploadByByte(fastDFSFile);  //upload to fastdfs
				path = fastDfsClient.getTrackerUrl() + fileAbsolutePath[0] + "/" + fileAbsolutePath[1];
				logger.debug("path:" + path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FastDFSException e) {
			e.printStackTrace();
		}
	}


	//@Test
	//public void nettyUploadTest() throws InterruptedException {
	//	int poolSize = 10;
	//	final CountDownLatch start = new CountDownLatch(1);
	//	final CountDownLatch end = new CountDownLatch(poolSize);
	//	ExecutorService exce = Executors.newFixedThreadPool(poolSize);
	//	for (int i = 0; i < poolSize; i++) {
	//		Runnable run = new Runnable() {
	//			@Override
	//			public void run() {
	//				try {
	//					start.await();
	//					testNetty();
	//				} catch (InterruptedException e) {
	//					e.printStackTrace();
	//				} finally {
	//					end.countDown();
	//				}
	//			}
	//		};
	//		exce.submit(run);
	//	}
	//	start.countDown();
	//	end.await();
	//	exce.shutdown();
	//}

	//private void testNetty() {
    //
	//	String path = "";
	//	String filePath = "C:\\Users\\Army\\Desktop\\2.png";
	//	File file = new File(filePath);
	//	try {
	//		byte[] fileBuff = null;
	//		for (int i = 0; i < 10000; i++) {
	//			FastdfsExecutor executor = new FastdfsExecutor();
	//			executor.init();
	//			InputStream inputStream = new FileInputStream(file);
	//			if (inputStream != null) {
	//				int len1 = inputStream.available();
	//				fileBuff = new byte[len1];
	//				inputStream.read(fileBuff);
	//			}
	//			String ext = filePath.substring(filePath.lastIndexOf(".") + 1);
	//			SimpleFastdfsClient dfs = getSimpleFastdfsClient(executor);
	//			filePath = "http://192.168.50.34:8088/dfs/" + dfs.upload(fileBuff, ext);
	//			logger.debug("filePath:" + filePath);
	//			executor.shutdown();
	//		}
	//	} catch (IOException e) {
	//		e.printStackTrace();
	//	} finally {
    //
	//	}
	//}

	private List<InetSocketAddress> getTrackers() {
		List<InetSocketAddress> list = new ArrayList();
		String spr1 = ",";
		String spr2 = ":";
		String[] arrs = trackerServers.trim().split(spr1);
		String[] var = arrs;
		int len = arrs.length;
		for (int i = 0; i < len; ++i) {
			String addrStr = var[i];
			String[] address = addrStr.trim().split(spr2);
			String host = address[0].trim();
			int port = Integer.parseInt(address[1].trim());
			list.add(new InetSocketAddress(host, port));
		}
		return list;
	}

	//private SimpleFastdfsClient getSimpleFastdfsClient(FastdfsExecutor client) {
	//	return new SimpleFastdfsClient(client, getTrackers());
	//}

	//@Test
	//public void testcyclicBarrier() {
	//	int count = 500;
	//	CyclicBarrier cyclicBarrier = new CyclicBarrier(count);
	//	ExecutorService executorService = Executors.newFixedThreadPool(count);
	//	for (int i = 0; i < count; i++)
	//		executorService.execute(new GxdComponentsFastdfsApplicationTests().new Task(cyclicBarrier));
    //
	//	executorService.shutdown();
	//	while (!executorService.isTerminated()) {
	//		try {
	//			Thread.sleep(10);
	//		} catch (InterruptedException e) {
	//			e.printStackTrace();
	//		}
	//	}
	//}

	//public class Task implements Runnable {
	//	private CyclicBarrier cyclicBarrier;
    //
	//	public Task(CyclicBarrier cyclicBarrier) {
	//		this.cyclicBarrier = cyclicBarrier;
	//	}
    //
	//	@Override
	//	public void run() {
	//		try {
	//			// 等待所有任务准备就绪
	//			cyclicBarrier.await();
	//			// 测试内容
	//			testNetty();
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//		}
	//	}
	//}

	@Test
	public void testPerformance() {
		final AtomicInteger failCount = new AtomicInteger(0);
		final AtomicInteger count = new AtomicInteger(0);
		int totalCount = 1000;
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(100);
		executor.afterPropertiesSet();
		String filePath = "C:\\Users\\Army\\Desktop\\servererror.png";
		for (int i = 0; i < totalCount; i++) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					try {
						byte[] bytes = FileUtils.readFileToByteArray(new File(filePath));
						StorePath storePath = storageClient.uploadFile(null, new ByteArrayInputStream(bytes), bytes.length, "jpg");
						System.out.println("storePath: " + storePath.getFullPath());
					} catch (Exception e) {
						e.printStackTrace();
						failCount.incrementAndGet();
					} finally {
						count.incrementAndGet();
					}
				}
			});
		}
		while (count.get() < totalCount) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				return;
			}
		}
		executor.destroy();
		System.out.println("success count: " + count.get());
		System.out.println("fail count: " + failCount.get());
	}



	private static int thread_num = 400;
	private static int client_num = 500;
	//@Test
	//public void test() {
	//	ExecutorService exec = Executors.newCachedThreadPool();
	//	// thread_num个线程可以同时访问
	//	final Semaphore semp = new Semaphore(thread_num);
	//	// 模拟client_num个客户端访问
	//	for(int index = 0; index<client_num;index++) {
	//		final int NO = index;
	//		Runnable run = new Runnable() {
	//			public void run() {
	//				try {
	//					// 获取许可
	//					semp.acquire();
    //
	//					System.out.println("Thread并发事情>>>" + NO);
	//					for (int i = 0; i < 10000; i++) {
	//						testUpload2();
	//					}
	//					System.out.println(NO);
	//					semp.release();
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
	//		};
	//		exec.execute(run);
	//	}
	//	// 退出线程池
	//		exec.shutdown();
	//}

	//private  void testUpload2() {
	//	FastdfsExecutor executor = new FastdfsExecutor();
	//	executor.init();
	//	try {
	//		SimpleFastdfsClient dfs = getSimpleFastdfsClient(executor);
	//		String imageuri = testUploadImage(dfs);
	//		System.out.println("imageuri:"+imageuri);
	//	} finally {
	//		executor.shutdown();
	//	}
	//}
    //
	//private  String testUploadImage(SimpleFastdfsClient dfs) {
	//	String filePath="C:\\Users\\Army\\Desktop\\2.png";
	//	File file = new File(filePath);
	//	try {
	//		byte[] fileBuff = null;
	//		InputStream inputStream = new FileInputStream(file);
	//		if (inputStream != null) {
	//			int len = inputStream.available();
	//			fileBuff = new byte[len];
	//			inputStream.read(fileBuff);
	//		}
	//		return dfs.upload(fileBuff, "png");
    //
	//	}catch (Exception e){
	//		e.getStackTrace();
	//	}
	//	return filePath;
	//}

	//public static void main(String[] args) {
	//	final int count = 80; // 计数次数
	//	final CountDownLatch latch = new CountDownLatch(count);
	//	for (int i = 0; i < count; i++) {
	//		new Thread(new Runnable() {
	//			@Override
	//			public void run() {
	//				try {
	//					// do anything
	//					for (int i = 0; i < 100000; i++) {
	//						uploadByPython();
	//					}
	//					System.out.println("线程"
	//							+ Thread.currentThread().getId());
	//				} catch (Throwable e) {
	//					// whatever
	//				} finally {
	//					// 很关键, 无论上面程序是否异常必须执行countDown,否则await无法释放
	//					latch.countDown();
	//				}
	//			}
	//		}).start();
	//	}
	//	try {
	//		// 线程countDown()都执行之后才会释放当前线程,程序才能继续往后执行
	//		latch.await();
	//	} catch (InterruptedException e) {
	//		// whatever
	//	}
	//	System.out.println("Finish");
	//}
}
