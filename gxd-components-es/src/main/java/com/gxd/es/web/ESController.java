package com.gxd.es.web;

import com.gxd.commons.model.Result;
import com.gxd.es.service.AddressCaseByEs;
import com.gxd.es.service.AddressNavByEs;
import com.gxd.es.service.AddressUserAccessByEs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.*;

/**
 * @Author:gxd
 * @Description:
 * @Date: 14:50 2018/1/31
 * @Modified By:
 */
@Controller
@RequestMapping("/es")
public class ESController {
    private static final Logger logger = LoggerFactory.getLogger(ESController.class);

    @Autowired
    private AddressNavByEs addressNavByEs;
    @Autowired
    private AddressUserAccessByEs addressUserAccessByEs;
    @Autowired
    private AddressCaseByEs addressCaseByEs;

    /**
     * 小区搜索接口
     * @param name
     * @param cityCode
     * @return
     */
    @RequestMapping(value = "/getCommunityInfo")
    @ResponseBody
    public Result getCommunityInfo(String name,String cityCode){
        logger.debug("小区搜索接口参数getCommunityInfo name:{},cityCode:{}",name,cityCode);
        System.out.println("小区搜索接口参数getCommunityInfo name:"+name+",cityCode:"+cityCode);
        Result result = new Result();
        HashSet<String> search = new HashSet<>();
        try {

            search = addressNavByEs.search(name, cityCode);
            System.out.println("小区search:"+search.toString());
            result.setPage(search.size());
            result.setState(Result.SUCCESS);
            result.setSuccess(Boolean.TRUE);
            result.setMessage("success");
            result.addAttribute("communityInfo",search);
        }catch (Exception e){
            result.setPage(0);
            result.setState(-1);
            result.setSuccess(Boolean.FALSE);
            result.setMessage("fail");
            result.addAttribute("communityInfo",e.getMessage());
        }
        return result;
    }

    /**
     * 案例接口
     * @param paramsCase
     * @return
     */
    @RequestMapping(value = "/getAddressCase")
    @ResponseBody
    public Result getAddressCase(@RequestParam Map paramsCase){
        System.out.println("案例接口参数getAddressCase:"+paramsCase.toString());
        Result result = new Result();
        Map<String,Object> search = new HashMap<>();
        try {
            search = addressCaseByEs.search(paramsCase);
            System.out.println("案例search:"+search.toString());
            result.setPage(search.size());
            result.setState(Result.SUCCESS);
            result.setSuccess(Boolean.TRUE);
            result.setMessage("success");
            result.addAttribute("addressCase",search);
        }catch (Exception e){
            result.setPage(0);
            result.setState(-1);
            result.setSuccess(Boolean.FALSE);
            result.setMessage("fail");
            result.addAttribute("addressCase",e.getMessage());
        }

        return result;
    }

    /**
     * 用户访问日志接口
     * @param paramsCase
     * @return
     */
    @RequestMapping(value = "/getUserAccessLog")
    @ResponseBody
    public Result getUserAccessLog(@RequestParam Map paramsCase){
        Result result = new Result();
        System.out.println("用户访问日志接口参数getUserAccessLog:"+paramsCase.toString());

        List search = new ArrayList();
        try {
            search = addressUserAccessByEs.search(paramsCase);
            System.out.println("用户访问search:"+search.toString());
            result.setPage(search.size());
            result.setState(Result.SUCCESS);
            result.setSuccess(Boolean.TRUE);
            result.setMessage("success");
            result.addAttribute("userAccessLog",search);
        }catch (Exception e){
            result.setPage(0);
            result.setState(-1);
            result.setSuccess(Boolean.FALSE);
            result.setMessage("fail");
            result.addAttribute("userAccessLog",e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/getCommunityInfo2")
    @ResponseBody
    public Result getCommunityInfo2(String name,String cityCode){
        Result result = new Result();
        HashSet<String> search = null;
        try {
            search = addressNavByEs.testSearchAddress(name,cityCode);
        } catch (ParseException e) {
            e.printStackTrace();
            result.setPage(search.size());
            result.setState(-1);
            result.setSuccess(Boolean.FALSE);
            result.setMessage("false");
            result.addAttribute("communityInfo2",search);
        }
        result.setPage(search.size());
        result.setState(Result.SUCCESS);
        result.setSuccess(Boolean.TRUE);
        result.setMessage("success");
        result.addAttribute("communityInfo2",search);
        return result;
    }
}
