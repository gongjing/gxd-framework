package com.gxd.es.service;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @Author:gxd
 * @Description:
 * @Date: 12:00 2018/4/16
 * @Modified By:
 */



@Component
public class AddressCaseByEs {


    public Map<String,Object>  search(Map<String,String> paramers) throws Exception{

        ElasticTransportClient elasticTransportClient = new ElasticTransportClient();
        long startime = System.currentTimeMillis();
        SearchResponse searchResponse = elasticTransportClient.caseSearch(paramers);

        Map<String,Object> resultMap = new HashMap<>();
        List<Map>  rowsMap = new ArrayList<>();

        Long total = searchResponse.getHits().getTotalHits();
        for (SearchHit hit : searchResponse.getHits().getHits()) {
            Map<String, Object> indexMap = hit.getSourceAsMap();
            System.out.println(indexMap.toString());
            rowsMap.add(indexMap);
        }

        resultMap.put("total",total);
        resultMap.put("rows",rowsMap);


        System.out.println(resultMap);
        System.out.println("查询共消耗时间：" + (System.currentTimeMillis() - startime) + "ms");

        return  resultMap;
    }

}