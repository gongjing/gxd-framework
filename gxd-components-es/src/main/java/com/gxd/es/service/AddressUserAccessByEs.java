package com.gxd.es.service;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.filter.InternalFilter;
import org.elasticsearch.search.aggregations.bucket.histogram.InternalDateHistogram;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AddressUserAccessByEs {


    public List<Map<String,Object>>  search(Map<String,String> paramers) {

         /*
         * list结果集
         * */

         List<Map<String,Object>> resultList = new ArrayList<>();

        ElasticTransportClient ElasticTransportClient = new ElasticTransportClient();
        long startime = System.currentTimeMillis();
        SearchResponse SearchResponse = ElasticTransportClient.userAccessSearch(paramers);

        Map<String, Aggregation> aggMap = SearchResponse.getAggregations().asMap();
        StringTerms resourceurlTerms = (StringTerms) aggMap.get("BY_RESOURCE_URL");
        Iterator<StringTerms.Bucket> resourceurlBucketIt = resourceurlTerms.getBuckets().iterator();

        while(resourceurlBucketIt.hasNext()) {


            String RESOURCE_URL = "";
            String RESOURCE_NAME = "";
            String ACCESS_LIMIT_LEVEL = "";
            String ACCESS_LIMIT_LEVELNAME = "";


            /**
             * 资源url
             */
            StringTerms.Bucket resourceurlBucket = resourceurlBucketIt.next();
            RESOURCE_URL = (String)resourceurlBucket.getKey();
           // System.out.println("资源URL："+resourceurlBucket.getKey() + "请求数量："+ resourceurlBucket.getDocCount());

            /**
             * 资源名称
             */
            StringTerms resourcenameTerms = (StringTerms) resourceurlBucket.getAggregations().asMap().get("BY_RESOURCE_NAME");
            Iterator<StringTerms.Bucket> resourcenameBucketIt = resourcenameTerms.getBuckets().iterator();
            StringTerms.Bucket resourcenameBucket = resourcenameBucketIt.next();
            RESOURCE_NAME = (String)resourcenameBucket.getKey();
            //System.out.println("资源Name："+resourcenameBucket.getKey() + "请求数量："+ resourcenameBucket.getDocCount());

            /**
             * 资源拦截级别
             */
            StringTerms accesslimitlevelTerms = (StringTerms) resourcenameBucket.getAggregations().asMap().get("BY_ACCESS_LIMIT_LEVEL");
            Iterator<StringTerms.Bucket> accesslimitlevelBucketIt = accesslimitlevelTerms.getBuckets().iterator();
            StringTerms.Bucket accesslimitlevelstaffBucket = accesslimitlevelBucketIt.next();
            ACCESS_LIMIT_LEVEL = (String)accesslimitlevelstaffBucket.getKey();
            if(accesslimitlevelstaffBucket.getKey().equals("0")){
                ACCESS_LIMIT_LEVELNAME ="底";
            }else if(accesslimitlevelstaffBucket.getKey().equals("1")){
                ACCESS_LIMIT_LEVELNAME = "中";
            }else{
                ACCESS_LIMIT_LEVELNAME ="高";
            }

            //System.out.println("访问级别："+accesslimitlevelstaffBucket.getKey() + "请求数量："+ accesslimitlevelstaffBucket.getDocCount());

            StringTerms staffTerms = (StringTerms) accesslimitlevelstaffBucket.getAggregations().asMap().get("BY_LOGIN_STAFF");
            Iterator<StringTerms.Bucket> staffBucketIt = staffTerms.getBuckets().iterator();

            while(staffBucketIt.hasNext()){

                /*
                 * 结果集的初始化*/
                Map<String,Object> resultMap = new HashMap<String,Object>();
                resultMap.put("RESOURCE_URL",RESOURCE_URL);
                resultMap.put("RESOURCE_NAME",RESOURCE_NAME);
                resultMap.put("ACCESS_LIMIT_LEVEL",ACCESS_LIMIT_LEVEL);
                resultMap.put("ACCESS_LIMIT_LEVELNAME",ACCESS_LIMIT_LEVELNAME);

                StringTerms.Bucket staffidBucket = staffBucketIt.next();
                resultMap.put("LOGIN_STAFF",staffidBucket.getKey());
               // System.out.println("员工ID："+staffidBucket.getKey() + "请求数量："+ staffidBucket.getDocCount());

                /**
                 * 员工名称
                 */
                StringTerms staffNameTerms = (StringTerms) staffidBucket.getAggregations().asMap().get("BY_STAFF_NAME");
                Iterator<StringTerms.Bucket> staffNameBucketIt = staffNameTerms.getBuckets().iterator();
                StringTerms.Bucket staffNameBucket = staffNameBucketIt.next();
                resultMap.put("LOGIN_NUMBER",staffNameBucket.getKey());
                resultMap.put("TOTAL",staffNameBucket.getDocCount());
               // System.out.println("员工Name："+staffNameBucket.getKey() + "请求数量："+ staffNameBucket.getDocCount());


                InternalDateHistogram timeTerms = (InternalDateHistogram) staffNameBucket.getAggregations().getAsMap().get("BY_REQUEST_DATE");
                Iterator<InternalDateHistogram.Bucket> timeBucketIt = timeTerms.getBuckets().iterator();


                /*
                 * 初始化时间段结果集*/
                resultMap.put("C0",0L);
                resultMap.put("C1",0L);
                resultMap.put("C2",0L);
                resultMap.put("C3",0L);
                resultMap.put("C4",0L);
                resultMap.put("C5",0L);
                resultMap.put("C6",0L);
                resultMap.put("C7",0L);
                resultMap.put("C8",0L);
                resultMap.put("C9",0L);
                resultMap.put("C10",0L);
                resultMap.put("C11",0L);
                resultMap.put("C12",0L);
                resultMap.put("C13",0L);
                resultMap.put("C14",0L);
                resultMap.put("C15",0L);
                resultMap.put("C16",0L);
                resultMap.put("C17",0L);
                resultMap.put("C18",0L);
                resultMap.put("C19",0L);
                resultMap.put("C20",0L);
                resultMap.put("C21",0L);
                resultMap.put("C22",0L);
                resultMap.put("C23",0L);
                //拦截次数
                Long filted = 0L;
                while(timeBucketIt.hasNext()){
                    InternalDateHistogram.Bucket timeBucket = timeBucketIt.next();
                    if (timeBucket.getKeyAsString().equals("00")){
                        resultMap.put("C0",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("01")){
                        resultMap.put("C1",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("02")){
                        resultMap.put("C2",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("03")){
                        resultMap.put("C3",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("04")){
                        resultMap.put("C4",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("05")){
                        resultMap.put("C5",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("06")){
                        resultMap.put("C6",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("07")){
                        resultMap.put("C7",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("08")){
                        resultMap.put("C8",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("09")){
                        resultMap.put("C9",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("10")){
                        resultMap.put("C10",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("11")){
                        resultMap.put("C11",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("12")){
                        resultMap.put("C12",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("13")){
                        resultMap.put("C13",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("14")){
                        resultMap.put("C14",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("15")){
                        resultMap.put("C15",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("16")){
                        resultMap.put("C16",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("17")){
                        resultMap.put("C17",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("18")){
                        resultMap.put("C18",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("19")){
                        resultMap.put("C19",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("20")){
                        resultMap.put("C20",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("21")){
                        resultMap.put("C21",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("22")){
                        resultMap.put("C22",timeBucket.getDocCount());
                    }else if(timeBucket.getKeyAsString().equals("23")){
                        resultMap.put("C23",timeBucket.getDocCount());
                    }else {

                    }
                    InternalFilter limitedTerms = (InternalFilter) timeBucket.getAggregations().getAsMap().get("BY_LIMIT_ID");
                    filted += limitedTerms.getDocCount();
                }
                resultMap.put("FILTEED",filted);
                /*
                * 国定参数值,方便和原有系统衔接
                * */
                resultMap.put("ROWNO",1);
                resultList.add(resultMap);
            }
        }

        System.out.println(resultList.toString());
        System.out.println("查询共消耗时间：" + (System.currentTimeMillis() - startime) + "ms");



        return  resultList;
    }

}
