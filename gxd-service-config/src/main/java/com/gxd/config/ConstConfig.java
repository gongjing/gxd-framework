package com.gxd.config;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Author:gxd
 * @Description:
 * @Date: 21:10 2018/4/12
 * @Modified By:
 */
@Component("constConfig")
@RefreshScope
public class ConstConfig {

    @Value("${gxd.sso.domainName:.test}")
    private String domainName;

    @Value("${gxd.sso.cookieMaxAge:604800}")
    private int cookieMaxAge;

    @Value("${gxd.sso.systemCode: systemamp}")
    private String systemCode;
    /**
     * 超级用户id
     */
    @Value("${gxd.sso.adminId: 0}")
    private String adminId;

        /**
     * key的有效期
     */
    @Value("${gxd.expiryDate: 30}")
    private int expiryDate;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public int getCookieMaxAge() {
        return cookieMaxAge;
    }

    public void setCookieMaxAge(int cookieMaxAge) {
        this.cookieMaxAge = cookieMaxAge;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }



    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }


    public int getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(int expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
